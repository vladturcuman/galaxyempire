using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class DragablePoision : MonoBehaviour {

	Vector2 startPoz,newPoz;
	public Vector2 finalPoz;
	public float dist,minRemoveDist;
	public Color startColor;
	public Color finalColor;
	public Image image;
	public delegate void Actions(DragablePoision gameObj);
	public event Actions OnRemove;

	void Start()
	{
		startPoz = transform.position;
	}

	public void BeginDrag()
	{
		CameraController.cameraController.enabled = false;
		StopAllCoroutines ();
	}

	public void Drag()
	{
		newPoz = Input.GetTouch(0).position;
		if (newPoz.y < startPoz.y && startPoz.y - newPoz.y <= dist) {
			transform.position = new Vector2 (startPoz.x, newPoz.y);
			image.color = Color.Lerp (startColor,finalColor,(startPoz.y-transform.position.y) / Mathf.Abs(startPoz.y - finalPoz.y));
		}
	}

	public void Drop()
	{
		if (startPoz.y - transform.position.y < minRemoveDist)
			StartCoroutine (MoveBack ());
		else
			StartCoroutine (MoveDown ());
		CameraController.cameraController.enabled = true;
	}

	IEnumerator MoveBack()
	{
		transform.position = new Vector2(startPoz.x,Mathf.Lerp(transform.position.y,startPoz.y,5 * Time.deltaTime));
		while (startPoz.y - transform.position.y > 1f) {
			image.color = Color.Lerp (startColor,finalColor,(startPoz.y-transform.position.y) / Mathf.Abs(startPoz.y - finalPoz.y));
			yield return null;
			transform.position = new Vector2(startPoz.x,Mathf.Lerp(transform.position.y,startPoz.y,5 * Time.deltaTime));
		}
		transform.position = startPoz;
		image.color = startColor;
	}

	IEnumerator MoveDown()
	{
		transform.position = new Vector2(startPoz.x,Mathf.Lerp(transform.position.y,finalPoz.y,5 * Time.deltaTime));
		while (Mathf.Abs(finalPoz.y - transform.position.y) > 1f) {
			image.color = Color.Lerp (startColor,finalColor,(startPoz.y-transform.position.y) / Mathf.Abs(startPoz.y - finalPoz.y));
			yield return null;
			transform.position = new Vector2(startPoz.x,Mathf.Lerp(transform.position.y,finalPoz.y,5 * Time.deltaTime));
		}
		transform.position = finalPoz;
		image.color = finalColor;
		if(OnRemove != null)
			OnRemove (this);
	}

	public void Disable()
	{
		gameObject.SetActive (false);
	}

	public void Refresh()
	{
		gameObject.SetActive (true);
		transform.position = startPoz; 
		image.color = startColor;
	}
}
