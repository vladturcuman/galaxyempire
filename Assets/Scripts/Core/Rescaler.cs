using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class Rescaler : MonoBehaviour
{
	IList<Transform> ObjectList = new List<Transform>();
	float maxDiference = 0.3f;
	float maxScale = 1.3f,minScale = 1f;
	float lastSize;

	void Start()
	{
		lastSize = Camera.main.orthographicSize;
    }

	public void AddToList(Transform newOne)
	{
		ObjectList.Add (newOne);
        Rescale(Camera.main.orthographicSize);
	}

	void Update()
	{
			if(Mathf.Abs (Camera.main.orthographicSize - lastSize) >= maxDiference)
			{
				if(Camera.main.orthographicSize > lastSize)
					Rescale(lastSize + maxDiference);
				else
					Rescale(lastSize - maxDiference);
			}
			else if(Camera.main.orthographicSize != lastSize)
				Rescale (Camera.main.orthographicSize);
	}

	void Rescale(float newSize)
	{
		for (int i=0;i<ObjectList.Count;i++) {
            float scale = minScale + (Camera.main.orthographicSize - CameraController.cameraController.minSize) *
                (maxScale - minScale) / (CameraController.cameraController.maxSize - CameraController.cameraController.minSize);
            ObjectList[i].localScale = new Vector2(scale, scale);
		}
		lastSize = newSize;
	}
}
