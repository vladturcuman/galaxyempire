using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Powers
{
    public class Ice : Photon.MonoBehaviour, IPower
    {
        public PowerTypes Type { get { return PowerTypes.Ice; } }
        public string Name { get { return "Ice Power"; } }
        public string SpritePath { get { return "Powers/Ice/icon"; } }
        public Character Character { get; set; }
        public bool isActive { get; set; }

        public event Action<IPower> OnStoped;

        public static float Time = 20;

        public void StartPow(Character Character)
        {
            isActive = true;
            this.Character = Character;

            Character.photonView.RPC("SetState", PhotonTargets.All, CharacterStates.Ice);
            Invoke("StopPow", Time);
        }

        public void StopPow()
        {
            if (!isActive)
                throw new Exception("Ice pow stopped & it was already stopped");

            isActive = false;
            
            Destroy(this);

            if (Character.State.Type == CharacterStates.Ice)
                Character.Normalize();

            if (OnStoped != null)
                OnStoped(this);
        }
    }
}
