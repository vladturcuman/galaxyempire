using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Powers
{
    public class Fire : Photon.MonoBehaviour, IPower
    {
        public PowerTypes Type { get { return PowerTypes.Fire; } }
        public string Name { get { return "Fire Power"; } }
        public string SpritePath { get { return "Powers/Fire/Icon"; } }
        public Character Character { get; set; }
        public bool isActive { get; set; }
        public event Action<IPower> OnStoped;

        public static float Time = 20;

        public void StartPow(Character character)
        {
            isActive = true;
            this.Character = character;
            Character.photonView.RPC("SetState", PhotonTargets.All, CharacterStates.Fire);
            Invoke("StopPow", Time);
        }

        public void StopPow()
        {
            if (!isActive)
                throw new Exception("Fire pow stopped & it was already stopped");

            isActive = false;

            Destroy(this);

            if (Character.State.Type == CharacterStates.Fire)
                Character.Normalize();

            if (OnStoped != null)
                OnStoped(this);
        }
    }
}
