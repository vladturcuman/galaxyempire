using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Powers
{
    public class PlanetMover : Photon.MonoBehaviour, IPower
    {
        public PowerTypes Type { get { return PowerTypes.PlanetMover; } }
        public string Name { get { return "PlanetMover"; } }
        public string SpritePath { get { return "Powers/PlanetMover/Icon"; } }
        public Character Character { get; set; }
        public bool isActive { get; set; }
        Planet Planet;
        float lastTime=-1;
        Vector2 pos;

        public event Action<IPower> OnStoped;

        public void StartPow(Character Character)
        {
            isActive = true;
            this.Character = Character;
            if (Character.IsManualControlled)
            {
                GameController.gameC.UnlightAllPlanets();

                InputManager.CurrentEvent = InputManager.EmptyEvent;
                CameraController.cameraController.minSize += 10;
                CameraController.cameraController.maxSize += 10;
                CameraController.cameraController.canExplore = false;
                CameraController.cameraController.Resize(CameraController.cameraController.maxSize, 2);
                InputManager.PlanetEvent.OnDrag += Drag;
                InputManager.PlanetEvent.OnDrop += Drop;
                InputManager.PlanetEvent.OnDoubleTap -= GameController.GoToPlanet;
                InputManager.MoveCharacter = false;
            }
            Character.PM.MovementPermission = false;
        }

        public void StopPow()
        {
            if (!isActive)
                return;

            isActive = false;
            try { CancelInvoke("StopPow"); } catch { }

            if (Character.IsManualControlled)
            {
                GameController.gameC.UnlightAllPlanets();

                if (Planet != null)
                    Planet.LightPlanet(0);
                CameraController.cameraController.minSize -= 10;
                CameraController.cameraController.maxSize -= 10;
                CameraController.cameraController.canExplore = true;
                CameraController.cameraController.Resize(CameraController.cameraController.maxSize, 2);
                InputManager.PlanetEvent.OnDrag -= Drag;
                InputManager.PlanetEvent.OnDrop -= Drop;
                InputManager.PlanetEvent.OnDoubleTap += GameController.GoToPlanet;
                InputManager.MoveCharacter = true;
            }

            Character.PM.MovementPermission = true;
            Planet = null;
            Destroy(this);

            if (OnStoped != null)
                OnStoped(this);
        }

        public void Drag(GameObject Planet, GameObject obj, Vector2 pos)
        {
            if (lastTime == -1)
            {
                this.Planet = Planet.GetComponent<Planet>();
                if (this.Planet.Type  == StationTypes.Sun)
                {
                    Planet = null;
                    return;
                }
                lastTime = Time.time;
                if(Character.IsManualControlled)
                    this.Planet.LightPlanet(3);
                Invoke("StopPow", 5f);
            }
            if (Time.time - lastTime > 0.1f)
            {
                lastTime = Time.time;
            }
            this.pos = pos;
        }

        void FixedUpdate()
        {
            if (Planet != null)
            {
                Planet.Transform.GetComponent<SmoothPosition>().photonView.RPC("RealPosition", PhotonTargets.MasterClient, pos);
            }
        }

        void Drop(GameObject Planet)
        {
               StopPow();
        }

        #region IPower
        

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        #endregion

    }
}
