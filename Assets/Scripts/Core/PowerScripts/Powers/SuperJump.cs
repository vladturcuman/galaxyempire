using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Powers
{
    public class SuperJump : MonoBehaviour, IPower
    {
        #region IPower

        public PowerTypes Type { get { return PowerTypes.SuperJump; } }
        public string Name { get { return "SuperJump"; } }
        public string SpritePath { get { return "Powers/SuperJump/Icon"; } }
        public Character Character { get; set; }
        public bool isActive { get; set; }
        public event Action<IPower> OnStoped;

        public void StartPow(Character Character)
        {
            isActive = true;
            this.Character = Character;
            jumpsNumber = 0;

            if (Character.IsManualControlled)
            {
                CameraController.cameraController.minSize += 10;
                CameraController.cameraController.maxSize += 10;
                CameraController.cameraController.Resize(CameraController.cameraController.maxSize, 2);
            }
            Character.PM.JumpDistance += 10;
            Character.PM.OnPArrive += CharacterArrived;
        }

        public void StopPow()
        {
            if (!isActive)
                return;

            isActive = false;
            if (Character.IsManualControlled)
            {
                CameraController.cameraController.maxSize -= 10;
                CameraController.cameraController.minSize -= 10;
                CameraController.cameraController.Resize(CameraController.cameraController.maxSize, 2);
            }
            Character.PM.JumpDistance -= 10;
            Character.PM.OnPArrive -= CharacterArrived;
            Destroy(this);
            if (OnStoped != null)
                OnStoped(this);
        }
        #endregion

        const int totalJumpsNumber = 5;
        int jumpsNumber = 0;

        void CharacterArrived(IStation from, IStation to)
        {
            if (++jumpsNumber >= totalJumpsNumber)
                StopPow();
        }
        
    }
}