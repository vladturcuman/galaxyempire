using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using UnityEngine;

public class PowerIcon : Photon.PunBehaviour
{
    public delegate void IconAction(PowerIcon moneyIcon);
    public event IconAction OnMoved;
    public static event IconAction OnAwaked;

    public float WaitTime = 20;
    public Planet Planet;
    public Type Type;
    public PowerTypes PowerType;

    SpriteRenderer spriteRenderer;

    public virtual void Awake()
    {
        Type = Type.GetType((string)photonView.instantiationData[1] + ", " + (string)photonView.instantiationData[0]);
        IPower pow = (IPower)Activator.CreateInstance(Type);
        gameObject.name = pow.Name;
        PowerType = pow.Type;
        WaitTime = ((PowerTypesValue)(typeof(PowerTypes).GetMember(PowerType.ToString())[0].GetCustomAttributes(true)[0])).WaitTime;
        GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>(pow.SpritePath);

        if (OnAwaked != null)
            OnAwaked(this);
    }

    public virtual void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        GameController.rescaler.AddToList(transform);
        if(PhotonNetwork.isMasterClient)
            InvokeRepeating("Move", 0, WaitTime);
    }
   
    public virtual void OnTriggerStay2D(Collider2D coll)
    {
        Character Character = coll.transform.parent.gameObject.GetComponent<Character>();
        if (Character != null && Character.PC != null)
        {
            Character.PC.AddPowToQue(PowerType);
            Move();
        }
    }
    
    [PunRPC]
    public void SetOn(int id)
    {
        transform.rotation = Quaternion.identity;
        Planet = Planet.Planets[id];
        transform.SetParent(Planet.Transform);
        transform.position = new Vector3(Planet.Transform.position.x, Planet.Transform.position.y + Planet.Radius * 2.5f, 0);
        if(OnMoved != null)
            OnMoved(this);
    }

    public virtual void Move()
    {
        photonView.RPC("SetOn", PhotonTargets.All, ((IEnumerable<Planet>)(Planet.Planets.Values)).PickRandom().ID);
    }

    public Sprite GetSprite()
    {
        return spriteRenderer.sprite;
    }
}
