

public class PowerTypesValue : System.Attribute
{
    public System.Type Type { get; set; }
    public float WaitTime { get; set; }

    public PowerTypesValue(System.Type type, float waitTime)
    {
        Type = type;
        WaitTime = waitTime;
    }
}