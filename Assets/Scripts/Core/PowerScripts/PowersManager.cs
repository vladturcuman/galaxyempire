using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using UnityEngine;

public static class PowersManager
{
    public static IDictionary<PowerTypes, PowerIcon> IconDex = new Dictionary<PowerTypes, PowerIcon>();

    public static void SetUp()
    {
        PowerIcon.OnAwaked += PowerIconAwaked;
    }

    static void PowerIconAwaked(PowerIcon powerIcon)
    {
        IconDex.Add(powerIcon.PowerType, powerIcon);
    }

    public static void LoadPowers()
    {
        var type = typeof(PowerTypes);
        foreach (PowerTypes power in Enum.GetValues(type))
        {
            Type t = ((PowerTypesValue)(type.GetMember(power.ToString())[0].GetCustomAttributes(true)[0])).Type;
            PhotonNetwork.Instantiate("Powers/icon", Vector3.zero, Quaternion.identity, 0, new object[] { t.Assembly.GetName().Name , t.FullName });
        }
    }

    public static void ClearData()
    {
        IconDex.Clear();
        PowerIcon.OnAwaked -= PowerIconAwaked;
    }

}