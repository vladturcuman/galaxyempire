using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public static class Extension
{
    //Breadth-first search
    public static Transform FindDeepChild(this Transform aParent, string aName)
    {
        var result = aParent.Find(aName);
        if (result != null)
            return result;
        foreach (Transform child in aParent)
        {
            result = child.FindDeepChild(aName);
            if (result != null)
                return result;
        }
        return null;
    }

    public static Vector2 GetUnitOncircle(float angleDegrees, float radius)
    {
        
        // initialize calculation variables
        float _x = 0;
        float _y = 0;
        float angleRadians = 0;
        Vector2 _returnVector;
        // convert degrees to radians
        angleRadians = angleDegrees * Mathf.PI / 180.0f;
        // get the 2D dimensional coordinates
        _x = radius * Mathf.Cos(angleRadians);
        _y = radius * Mathf.Sin(angleRadians);
        // derive the 2D vector
        _returnVector = new Vector2(_x, _y);
        // return the vector info
        return _returnVector;

    }
    
    public static T PickRandom<T>(this IEnumerable<T> source)
    {
        return source.PickRandom(1).Single();
    }

    public static IEnumerable<T> PickRandom<T>(this IEnumerable<T> source, int count)
    {
        return source.Shuffle().Take(count);
    }

    public static IEnumerable<T> Shuffle<T>(this IEnumerable<T> source)
    {
        return source.OrderBy(x => System.Guid.NewGuid());
    }
}

public class StringValue : System.Attribute
{
    private string _value;

    public StringValue(string value)
    {
        _value = value;
    }

    public string Value
    {
        get { return _value; }
    }

}

public class StationTypeValue : System.Attribute
{
    private string _path;
    private int _number;

    public StationTypeValue(int number, string path)
    {
        _number = number;
        _path = path;
    }

    public int Number
    {
        get { return _number; }
    }

    public string Path
    {
        get { return _path; }
    }
}
