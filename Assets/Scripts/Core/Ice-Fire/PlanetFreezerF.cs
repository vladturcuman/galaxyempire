using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using UnityEngine;

public class PlanetFreezerF : MonoBehaviour
{
    public float fireInterval = 10, iceInterval = 10, ffTime = 20;
    public float minIceDist = 25f, maxFireDist = 30f;

    void Awake()
    {
        StartCoroutine(FirePlanets());
        StartCoroutine(FreezPlanets());
    }

    IEnumerator FirePlanets()
    {
        FIPlanet Planet;
        int nr = 0;
        while (true)
        {
            yield return new WaitForSeconds(fireInterval);
            nr = 0;
            do
            {
                Planet = GameController.GetRandom(FIPlanet.FIPlanets);
                nr++;
            } while (!(Planet.DistanceFromSun <= maxFireDist && Planet.State == PlanetStates.Normal) && nr < 5);
            if (nr != 5)
                Planet.Fired(ffTime);
        }
    }
    
    IEnumerator FreezPlanets()
    {
        FIPlanet Planet;
        int nr = 0;
        while (true)
        {
            yield return new WaitForSeconds(fireInterval);
            nr = 0;
            do
            {
                Planet = GameController.GetRandom(FIPlanet.FIPlanets);
                nr++;
            } while (!(Planet.DistanceFromSun >= maxFireDist && Planet.State == PlanetStates.Normal) && nr < 5);
            if (nr != 5)
                Planet.Freez(ffTime);
        }
    }
}