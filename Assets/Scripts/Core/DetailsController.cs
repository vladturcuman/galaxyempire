using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DetailsController : MonoBehaviour {

	public CloseMenuBar MainMenu, PlanetsMenu, PowersMenu, MeMenu;
	public CloseMenuBar ActualMenu;

	public delegate void Action();
	public event Action OnExit;

	void Awake()
	{
		ActualMenu = MainMenu;
		MainMenu.OnExit += Exit;
		PlanetsMenu.OnExit += BackToMainMenu;
		PowersMenu.OnExit += BackToMainMenu;
		MeMenu.OnExit += BackToMainMenu;
	}

	public void PlanetsPressed()
	{
		if (ActualMenu == MainMenu && !ActualMenu.isMoving) {
			ActualMenu = PlanetsMenu;
			OpenClose ();
		}
	}

	public void PowersPressed()
	{
		if (ActualMenu == MainMenu && !ActualMenu.isMoving) {
			ActualMenu = PowersMenu;
			OpenClose ();
		}
	}

	public void MePressed()
	{
		if (ActualMenu == MainMenu && !ActualMenu.isMoving) {
			ActualMenu = MeMenu;
			OpenClose ();
		}
	}

	void OpenClose()
	{
		MainMenu.MoveUp ();
		Invoke ("OpenOtherMenu", 0.1f);
	}

	void OpenOtherMenu()
	{
		ActualMenu.transform.parent.gameObject.SetActive (true);
	}

	public void BackToMainMenu()
	{
		ActualMenu.transform.parent.gameObject.SetActive (false);
		MainMenu.transform.parent.gameObject.SetActive (true);
		ActualMenu = MainMenu;
	}

	void Exit()
	{
		if (ActualMenu == MainMenu) {
			if (OnExit != null)
				OnExit ();
			gameObject.SetActive (false);
		}
        else
	    	MainMenu.transform.parent.gameObject.SetActive (false);
	}
		
}
