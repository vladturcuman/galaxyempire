using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PathFinder
{
    struct PathS { public IStation p; public int ant; }
    public IList<IStation> Path = new List<IStation>();
    List<PathS> Que = new List<PathS>();
    bool[] isInQue = new bool[150];
    public PathFinderResult LastResult;
    Character Character;

    public PathFinder(Character character)
    {
        Character = character;
    }

    public PathFinderResult FindPath(IStation to)
    {
        if (Character.GetStation() == null)
            return LastResult = PathFinderResult.Failed;

        Refresh();

        if(Character.GetStation() == (object)to)
        {
            Path.Add(to);
            return LastResult = PathFinderResult.Succeded;
        }

        PathS aux, aux2;
        int poz = 0;
        aux.p = Character.GetStation(); aux.ant = -1;
        isInQue[aux.p.ID] = true;
        Que.Add(aux);

        while (poz < Que.Count)
        {
            aux = Que[poz];
            aux2.ant = poz;
            poz++;
            
            if (Character.PM.CanIGoTo(aux.p, to))
            {
                aux2.p = to;
                Que.Add(aux2);
                GetPath(Que.Count - 1);
                return LastResult = PathFinderResult.Succeded;
            }


            foreach (IStation next in GameController.Stations.Values)
            {
                if (next == aux.p)
                    continue;

                if (CanGoTo(aux.p, next))
                {
                    aux2.p = next;
                    isInQue[aux2.p.ID] = true;
                    Que.Add(aux2);
                }
            }

            if (poz > GameController.Stations.Count + 2)
                break;
        }

        return LastResult = PathFinderResult.Failed;
    }

    bool CanGoTo(IStation From, IStation To)
    {
        if(isInQue[To.ID])
            return false;
        return Character.PM.CanIGoTo(From, To);
    }

    internal virtual void Refresh()
    {
        Que.Clear();
        Path.Clear();
        for (int i = 0; i <= GameController.Stations.Count + 3; i++)
            isInQue[i] = false;
    }

    void GetPath(int poz)
    {
        if (poz != 0)
        {
            GetPath(Que[poz].ant);
            Path.Add(Que[poz].p);
        }
    }
}

public enum PathFinderResult
{
    Succeded,
    Forbidden,
    Failed
}