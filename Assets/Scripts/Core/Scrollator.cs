using UnityEngine;
using System.Collections;

public class Scrollator : MonoBehaviour 
{
    public RectTransform trs;
	public Vector2 minPos,maxPos,sum;
	public bool canMove;
	bool isMoving;
    public float space, speed;
	Vector2 boundMax,boundMin;

	void Awake()
	{
	    minPos = new Vector3 (transform.position.x, minPos.y);
		boundMin = new Vector3 (minPos.x, minPos.y - 100);
	}

    void Start()
    {
        Invoke("ArrangeItems",0.1f);
    }

    void ArrangeItems()
    {
        float height = 0;
        speed = trs.rect.height * 0.08f;
        space = trs.rect.height / 30;
        ScrollMenuItem[] GO = GetComponentsInChildren<ScrollMenuItem>();
        float nextDist = minPos.y + space, last = 0f;
        foreach(ScrollMenuItem item in GO)
        {
            last = nextDist;
            height = ((RectTransform)item.transform).rect.height;
            ((RectTransform)item.transform).offsetMin = new Vector2(0f, -nextDist);
            ((RectTransform)item.transform).offsetMax = new Vector2(0f, -nextDist);
            nextDist += height + space;
        }
        maxPos = new Vector3(transform.position.x, last - 2 * height);
        boundMax = new Vector3(maxPos.x, maxPos.y + 100);
    }

	void Update () 
	{
		if (!canMove)
			return;
		if (Input.touchCount > 0)
		    if (Input.GetTouch (0).phase == TouchPhase.Began)
		    {
			    isMoving = true;
			    StopAllCoroutines ();
		    }
		    else
		    if (Input.GetTouch (0).phase == TouchPhase.Moved && isMoving == true)
            {
			    sum = Input.GetTouch (0).deltaPosition;
			    sum = new Vector2 (0f, sum.y);
			    sum = trs.offsetMin + sum * Time.deltaTime * speed;
                float newPoz = Mathf.Clamp(sum.y, boundMin.y, boundMax.y);
                trs.offsetMin = new Vector2(0f, newPoz);
                trs.offsetMax = new Vector2(0f, newPoz);
		    }
            else
	            if (Input.GetTouch (0).phase == TouchPhase.Ended && isMoving)
                {
			        isMoving = false;
			        StartCoroutine (MoveBack(Mathf.Clamp(trs.offsetMin.y,minPos.y,maxPos.y)));
		        }
	}

	IEnumerator MoveBack(float pos)
	{
		trs.offsetMin = new Vector2(0f,Mathf.Lerp(trs.offsetMin.y,pos,5 * Time.deltaTime));
        trs.offsetMax = trs.offsetMin;
		while (Mathf.Abs(trs.offsetMin.y - pos) > 1f) {
			yield return null;
			trs.offsetMin = new Vector2(0f,Mathf.Lerp(trs.offsetMin.y,pos,5 * Time.deltaTime));
            trs.offsetMax = trs.offsetMin;
		}
        trs.offsetMin = new Vector2(0f, pos);
        trs.offsetMax = trs.offsetMin;
	}
}
