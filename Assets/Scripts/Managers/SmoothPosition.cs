using UnityEngine;
using System.Collections;

public class SmoothPosition : Photon.PunBehaviour {

	Vector3 realPosition = Vector3.zero;
	Quaternion realRotation = Quaternion.identity;
	public float lastUpdateTime = 0.1f;
	public bool TransformParent=false;
	Rigidbody2D RB;

	void Start()
	{
		RB = GetComponent<Rigidbody2D> ();
	}

	void FixedUpdate () {
		if (!photonView.isMine) {
			if (TransformParent) {
				transform.parent.position = Vector3.Lerp (transform.position, realPosition, lastUpdateTime);
				transform.parent.rotation = Quaternion.Lerp (transform.rotation, realRotation, lastUpdateTime);
				return;
			} 
			transform.position = Vector3.Lerp (transform.position, realPosition, lastUpdateTime);
			transform.rotation = Quaternion.Lerp (transform.rotation, realRotation, lastUpdateTime);

		}
	}

	[PunRPC] void RealPosition(Vector2 pos)
	{
        //RB.MovePosition(pos);
		RB.AddForce ((pos -(Vector2)transform.position)*2,ForceMode2D.Impulse); 
	}


	public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info){
		if(stream.isWriting){
			//This is OUR Character.We need to send our actual position to the network
			stream.SendNext(transform.position.x);
			stream.SendNext(transform.position.y);
			stream.SendNext(transform.rotation.eulerAngles.z); 
		}else{
			//This is someone else's Character.We need to receive their positin (as of a few millisecond ago, and update our version of that Character).
			realPosition.x = (float)stream.ReceiveNext();
			realPosition.y = (float)stream.ReceiveNext();
			realRotation = Quaternion.Euler(new Vector3(0,0,(float)stream.ReceiveNext()));
		}

	}

}
