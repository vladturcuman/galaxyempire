using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class InputManager : MonoBehaviour {

	const float TimeForDouble = 0.25f;
	const float MaxTapTime = 0.2f;

	public static bool MoveCharacter=true;

	private static Touch currentTouch;
	private static Vector2 v2;
	private static GameObject OnObject;
	private static RaycastHit2D[] TmpObjects;
	
	struct aTouch{
		public bool exist;
		public GameObject OnObject;
		public float Began;
		public float Ended;


	};

	public struct TouchEvents{
		public delegate void Tap(GameObject gObj );
		public delegate void DoubleTap( GameObject gObj);
		public delegate void Drag(GameObject gObj,GameObject obj,Vector2 pos);
		public delegate void Drop(GameObject gObj);

		public event Tap OnTap;
		public event DoubleTap OnDoubleTap;
		public event Drag OnDrag;
		public event Drop OnDrop;

		public void LaunchTap(GameObject sender)
		{
			if(OnTap!=null)
				OnTap (sender);	
		}
		public void LaunchDoubleTap(GameObject sender)
		{
			if(OnDoubleTap!=null)
				OnDoubleTap (sender);	
		}
		public void LaunchDrag(GameObject sender,GameObject a, Vector2 pos)
		{
			if(OnDrag!=null)
				OnDrag (sender,a,pos);	
		}
		public void LaunchDrop(GameObject sender)
		{
			if(OnDrop!=null)
				OnDrop (sender);	
		}
		public void Clear()
		{
			OnDrag = null;
			OnDrop = null;
			OnTap = null;
			OnDoubleTap = null;
		}
	};

	public static TouchEvents CharacterEvent; 
	public static TouchEvents PlanetEvent;
	public static TouchEvents CurrentEvent;
	public static TouchEvents EmptyEvent;
	
	static aTouch LastTap,CurrentTap;
    public static InputManager inputManager;

    void Awake()
    {
        inputManager = this;
    }

	void Update()
	{
		TouchCheck ();
		if (LastTap.exist )
        {
            if (Time.time-LastTap.Ended>=TimeForDouble)
			{
				try{
					CurrentEvent.LaunchTap(LastTap.OnObject);
				}catch{}
				
				LastTap.exist=false;
			}
		}
	}
	
	public static void TouchCheck()
	{

        if (Input.touchCount == 1) 
		{
            currentTouch = Input.GetTouch (0);
			v2 = (Vector2) Camera.main.ScreenToWorldPoint (currentTouch.position);
			
			OnObject = null;

			TmpObjects = Physics2D.RaycastAll(v2,Vector2.zero);

			foreach (RaycastHit2D r in TmpObjects) {
				try {
					if(MoveCharacter && r.collider.transform.parent.GetComponent<CharacterMover>()!=null)
					{
						OnObject = r.collider.transform.parent.gameObject;
						break;
					}
					if(!MoveCharacter && r.collider.GetComponent<IStation>()!=null)
					{
						OnObject = r.collider.gameObject;
						break;
					}
				} catch {
				}
			}


			if (OnObject == null) 
				foreach (RaycastHit2D r in TmpObjects) 
					try {
					if (r.collider.GetComponent<IStation>()!=null) {
							OnObject = r.collider.gameObject;
							break;
						}
					} catch {
					}

			
		
			Debug.DrawLine(v2, new Vector3(v2.x,v2.y,-10));

          
            if (currentTouch.phase == TouchPhase.Began) 
			{
           
                if (OnObject != null)
				{
                    CurrentTap.exist = true;
                	CurrentTap.Began = Time.time;
					
					CurrentTap.OnObject = OnObject.gameObject;

				

                }


            }
            else
			{

                if (!CurrentTap.exist) return;


				if (CurrentTap.OnObject.GetComponent<CharacterMover> () != null) {
					CurrentEvent = CharacterEvent;
				} 
				if (CurrentTap.OnObject.GetComponent<Planet> () != null) {
					CurrentEvent = PlanetEvent;
				} 

                if (currentTouch.phase == TouchPhase.Ended)
                {

                    try
                    {
                        CurrentEvent.LaunchDrop(CurrentTap.OnObject);
                    }
                    catch { }
                    if (OnObject != null)
                        if (CurrentTap.OnObject == OnObject)
                            if (Time.time - CurrentTap.Began <= MaxTapTime)
                                if (LastTap.exist)
                                {
                                    if (LastTap.OnObject == CurrentTap.OnObject)
                                    {
                                        try
                                        {
                                            CurrentEvent.LaunchDoubleTap(CurrentTap.OnObject);	
                                        }
                                        catch { }
                                    }
                                    LastTap.exist = CurrentTap.exist = false;
                                }
                                else
                                {
                                    CurrentTap.Ended = Time.time;
                                    LastTap = CurrentTap;
                                    CurrentTap.exist = false;
                                }
                            else
                                LastTap.exist = CurrentTap.exist = false;
                        else
                        {
							LastTap.exist = CurrentTap.exist = false;
                        }
                    else
                    {
                        LastTap.exist = CurrentTap.exist = false;
                    }

					CurrentTap.OnObject = null;
                }
                else
				{
					CurrentEvent.LaunchDrag(CurrentTap.OnObject,OnObject, v2); 
				}

			}
		} 
	}

	void OnDestroy()
	{
		CharacterEvent.Clear ();
		PlanetEvent.Clear ();

	}

}
