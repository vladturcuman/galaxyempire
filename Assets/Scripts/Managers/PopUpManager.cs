using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PopUpManager : MonoBehaviour {
	 
	private Text TextField;
	private Image ImageField;

	public delegate void MessageBody();
	public event MessageBody CloseDialog;

	public static PopUpManager popupmanager;

	void Awake()
	{
		popupmanager = this;
		TextField = GetComponentInChildren<Text> ();
		ImageField = GetComponentsInChildren<Image> ()[1];
		Close ();
	}

	public void Close()
	{
		if (CloseDialog != null)
			CloseDialog ();
		transform.parent.gameObject.SetActive (false);

	}

	public void SetUp(string s,string ImagePath)
	{
		transform.parent.gameObject.SetActive (true);
		ImageField.sprite = (Sprite) Resources.Load (ImagePath, typeof(Sprite));
		TextField.text = s;
	}
}
