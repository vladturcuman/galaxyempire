using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.CodeDom;

using UnityEngine;

public interface ICharacterBody
{
    void SetCharacter(Character Character);
    void ChangeWalkingState(bool value);
}