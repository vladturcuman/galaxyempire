using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public interface IFIPlanet
{
    PlanetStates State { get; }

    void Freez();
    void Fired();
    [PunRPC]
    void Freez(float time);
    [PunRPC]
    void Fired(float time);
    void Normalize();
    void TryNormalize(float after);
    bool CanStayOn(IFICharacter Character);
}
