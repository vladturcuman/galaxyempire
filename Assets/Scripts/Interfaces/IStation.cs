using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public interface IStation
{
    float Radius { get; }
    int ID { get;}
    Transform Transform { get; }
	int TrailCost{get;}
    StationTypes Type { get; }
    PlanetStates State { get; }
    IList<Character> Visitors { get; }

    void CharacterArrive(Character Character);
    void CharacterLeave(Character Character);

    bool CanStayOn(Character Character);
}


public enum StationTypes
{
    [StationTypeValue(20, "Planets/Flower/planet")]
    Flower,
    [StationTypeValue(10, "Planets/City/planet")]
    City,
    [StationTypeValue(10, "Planets/Candy/planet")]
    Candy,
    [StationTypeValue(1, "Planets/Sun/planet")]
    Sun,
    [StationTypeValue(1, "Planets/Finish/planet")]
    Finish,
    [StationTypeValue(1, "Planets/Points/planet")]
    Points
}