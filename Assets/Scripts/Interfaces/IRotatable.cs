using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public interface IRotatable
{
    Transform Transform { get; }
    Rigidbody2D RigidBody { get; }
}
