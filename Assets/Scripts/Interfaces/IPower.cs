using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Powers;

public interface IPower
{
    PowerTypes Type { get; }
    string Name { get; }
    string SpritePath { get; }
    bool isActive { get; }
    Character Character { get; set; }

    event Action<IPower> OnStoped;
    
    void StartPow(Character Character);
    void StopPow();
}

public enum PowerTypes
{
    [PowerTypesValue(typeof(Ice), 20f)]
    Ice,
    [PowerTypesValue(typeof(Fire), 20f)]
    Fire,
    [PowerTypesValue(typeof(SuperJump), 10f)]
    SuperJump,
    [PowerTypesValue(typeof(PlanetMover), 15f)]
    PlanetMover
}