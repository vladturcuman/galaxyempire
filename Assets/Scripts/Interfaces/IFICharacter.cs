using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public interface IFICharacter
{
    CharacterState State { get; }

    void Freez();
    void Fired(); 
    void Normalize();
}