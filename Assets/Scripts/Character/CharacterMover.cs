using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CharacterMover : Photon.MonoBehaviour {

    public float Angle;
	public float hopHeight = 1.25f;
	public float TimeToJump = 0.5f;
	public float AngleToRun = 10f;
	public float WalkSpeed;
	public float RunSpeed;
    public float JumpDistance = 20;

    public float NormalWalkSpeed;
    public float NormalRunSpeed;

    public bool CameraFollow=false;
	public bool ShowChoices=false;
	public bool HaveToGoSomewhere ;
	public bool MovementPermission{get{return _movePerm;}
		set{
            if (_movePerm != value)
            {
                if (!value)
                {
                    Que.Clear();
                    try { StopCoroutine("Rotate"); Message = null; } catch { }
                    try { Message -= Ready; } catch { }
                    HaveToGoSomewhere = false;
                }
                _movePerm = value;
                if (OnMovementPermisionChanged != null)
                    OnMovementPermisionChanged(value);
            }
        }
    }
    
	public Transform Anchor;
    public Transform tmp;
    
	public delegate void ArriveBody(Character Character);
	public delegate void ActionBody(IStation From,IStation To);
    public delegate void MovementPermisionAction(bool canMove);

	public event System.Action Message;
	public event ArriveBody ArriveEvent;
	public event ActionBody OnFArrive;
	public event ActionBody OnPArrive;
    public event System.Action<IStation> OnPLeave;
    public event System.Action OnEjected;
    public event MovementPermisionAction OnMovementPermisionChanged;

	public float Radius;
	private float Direction=-1; 

	public IStation PAttemptStation;
	public IStation PCurrentStation;
	public IStation PLastStation;
	public IStation FCurrentStation;
	public IStation FLastStation;

    public Character Character;

	bool _movePerm = true;
    private bool Ejected=false;

	private Queue<IStation> Que = new Queue<IStation>();

	public PathFinder pathFinder;
   
    void Awake()
    {
        tmp = (new GameObject()).GetComponent<Transform>();
    }

    public void SetCharacter(Character character)
    {
        if(Character != null)
        {
            Character.OnManualControlChanged -= EnableDisableManualMove;
            DisableManualMove();
            OnPArrive -= Character.Arrive;
            OnPLeave -= Character.Leave;
        }

        Character = character;
        Character.OnManualControlChanged += EnableDisableManualMove;
        pathFinder = new PathFinder(Character);
        OnPArrive += Character.Arrive;
        OnPLeave += Character.Leave;
        if (Character.IsManualControlled && enabled)
            EnableManualMove();
    }

    void EnableDisableManualMove(Character character)
    {
        if (Character.IsManualControlled && enabled)
            EnableManualMove();
        else
            if(!Character.IsManualControlled && enabled)
            DisableManualMove();
    }

    void EnableManualMove()
    {
        try {
            InputManager.CharacterEvent.OnDrag += Drag;
            InputManager.CharacterEvent.OnDrop += Drop;
        }
        catch { }
    }

    void DisableManualMove()
    {
        try {
            InputManager.CharacterEvent.OnDrag -= Drag;
            InputManager.CharacterEvent.OnDrop -= Drop;
        }catch { }
    }

    void OnEnable()
	{
        if (Character != null && Character.IsManualControlled)
            EnableManualMove();

        ArriveEvent += Arrive;
     }

	void OnDisable()
	{
        if (Character.IsManualControlled)
            DisableManualMove();

        ArriveEvent -= Arrive;
		OnEjected = null;
		OnMovementPermisionChanged = null;
		OnPArrive = null;
		OnFArrive = null;
    
    }

    #region FunctionsSection

    public void GoBack()
	{
		Que.Clear();
        Ejected = true;
		if (PLastStation != null)
			GoTo (PLastStation);
		else
			FindAnOther ();
	}

	public void FindAnOther()
	{
        Debug.Log("find other");
		if (HaveToGoSomewhere) {
			StopAllCoroutines ();
			HaveToGoSomewhere = false;
		}
		Que.Clear();
		Anchor = null;
		Character.photonView.RPC("ChangeParent",PhotonTargets.All, -1);
		Collider2D[] Colliders = Physics2D.OverlapCircleAll(transform.position, 20);
		foreach (Collider2D coll in Colliders)
			if (coll.gameObject.tag == "Planet" && CanIGoTo(coll.gameObject.GetComponent<IStation>()))
			{
				GoTo(coll.gameObject.GetComponent<IStation>());
				return;
			}
		foreach (Collider2D coll in Colliders)
			if (coll.gameObject.tag == "Planet")
			{
				GoTo(coll.gameObject.GetComponent<IStation>());
				return;
			}
	}

	public void SetOn(IStation p)
	{
		transform.rotation = Quaternion.identity;
		Radius = p.Radius;
		Que.Clear ();
		if (HaveToGoSomewhere) {
			StopAllCoroutines ();
			HaveToGoSomewhere = false;
		}
		PLastStation = PCurrentStation;
		PCurrentStation = p;
		if (PhotonNetwork.inRoom)
			Character.photonView.RPC("ChangeParent", PhotonTargets.All, p.ID);
		else
			transform.parent = p.Transform;
		Anchor = transform.parent;
		transform.localPosition = new Vector3(0f, Radius);
        if(OnPArrive != null)
            OnPArrive(PLastStation, PCurrentStation);
	}



	#endregion

	#region Drag`n`DropSection

	public void Drag(GameObject sender,GameObject On, Vector2 Position)
	{
        if (sender != gameObject)
            return;
        if (FCurrentStation == null)
            FCurrentStation = PCurrentStation;
        IStation Target = On != null ? On.GetComponent<IStation>() : null;

        if (On == null || Target == null )
		{
			if (Que.Count == 0 && MovementPermission && !HaveToGoSomewhere)
			{
				RotateTo(Position);
			}
			return;
		}
		if (FCurrentStation == Target)
            if(!CanIGoTo (FCurrentStation, Target))
			    return;


		FLastStation = FCurrentStation;
		FCurrentStation = Target;
		if (PCurrentStation == FCurrentStation && !HaveToGoSomewhere)
			return;

        if (OnFArrive != null && FLastStation != null) {
			OnFArrive (FLastStation, FCurrentStation);
		}
		GoTo(FCurrentStation);
	}

	public void Drop(GameObject sender)
	{
        if (sender != Character.gameObject)
            return;
        FLastStation = FCurrentStation = null;
	}
	#endregion

	#region HopSection

	public void DeleteJunk()
	{
		Que.Clear ();
        try { StopCoroutine("Rotate"); } catch { }
        //Message = null;
        HaveToGoSomewhere = false;
      //  StopAllCoroutines();
	}

    public void GoToPathFinderList()
    {
        GoToList(pathFinder.Path);
    }

    public void GoToList(IList<Planet> Planets)
    {
        GoToList((IList<IStation>)Planets);
    }

    public void GoToList(IList<IStation> Stations)
	{
		Que = new Queue<IStation> (Stations);

		if (Stations.Count == 0)
			return;

		if (MovementPermission) {
			HaveToGoSomewhere = true;
			PAttemptStation = Que.Dequeue ();
			StopAllCoroutines ();
			
			if (Anchor != null) {
				Message += Ready;
				RotateTo (PAttemptStation.Transform, true);
			} else
				Ready ();
		} else {
			Que.Clear ();
			HaveToGoSomewhere = false;
		}
	}

	public void GoTo(IStation p)
	{
		Que.Enqueue(p);


		if (Que.Count == 1 && !HaveToGoSomewhere && MovementPermission)
		{
			HaveToGoSomewhere=true;
			PAttemptStation = Que.Dequeue();
			
			if (Anchor != null) {
				Message += Ready;
				RotateTo (PAttemptStation.Transform, true);
			}
			else
				Ready();
		}
		if (MovementPermission)
			Que.Clear ();
	}

	public void Arrive(Character Character)
	{
		PCurrentStation = PAttemptStation;

        HaveToGoSomewhere = false;

        if (Ejected)
        {
            Ejected = false;
            if (OnEjected != null)
                OnEjected();
        }

		if(transform.parent!=null)
			Anchor = transform.parent;

        if (OnPArrive != null)
            OnPArrive(PLastStation, PCurrentStation);

        do
        {
            if (!MovementPermission || Que.Count == 0)
            {
                Que.Clear();
                return;
            }

            PAttemptStation = Que.Dequeue();
        } while (PAttemptStation == PCurrentStation);


        HaveToGoSomewhere = true;

		if (Anchor != null) {
			Message += Ready;
			RotateTo (PAttemptStation.Transform, true);
		}
		else
			Ready();


	}

    void Ready()
    {
        try { Message -= Ready; } catch { }

        if (PCurrentStation == PAttemptStation)
        {
            //Arrive(Character);
            HaveToGoSomewhere = false;
            return;
        }

        if (GameController.gameC != null)
            if (PCurrentStation != null)
            {
                // if (Vector3.Distance(PCurrentStation.Transform.position, PAttemptStation.Transform.position) > JumpDistance * 1.5)
                if(!CanIGoTo(PAttemptStation))
                {
                    Que.Clear();
                    HaveToGoSomewhere = false;
                    return;
                }
            }

        PLastStation = PCurrentStation;
		PCurrentStation = null;

		if (GameController.gameC != null)
			Character.photonView.RPC("ChangeParent",PhotonTargets.AllBuffered,PAttemptStation.ID);
		else
			transform.SetParent (PAttemptStation.Transform);
		Radius = PAttemptStation.Radius;

		float Angle = GetOriginAngle(transform.localPosition);

		HaveToGoSomewhere = true;

        if(OnPLeave != null)
            OnPLeave(PLastStation);

		StopAllCoroutines();
		StartCoroutine(Hop(CalculatePosition(Angle), 
        Quaternion.Euler(0f, 0f,Angle + 270) ) );
	}

    public bool CanIGoTo(IStation To)
	{
        return PCurrentStation != To && Vector2.Distance(transform.position, To.Transform.position) <= JumpDistance + To.Radius && To.CanStayOn(Character) && Character.CanStayOn(To);
    }

    public bool CanIGoTo(IStation From, IStation To)
    {
        return From != To && Vector2.Distance(From.Transform.position, To.Transform.position) <= JumpDistance + To.Radius + From.Radius && To.CanStayOn(Character) && Character.CanStayOn(To);
    }

    float GetOriginAngle(Vector2 Point)
	{
		float angle1 = Mathf.Atan2(0, 1);
		float angle2 = Mathf.Atan2(Point.y, Point.x);

		float _result = ((float)(angle2 - angle1)) * 180 / 3.14f;

		if (_result < 0)
		{
			_result += 360;
		}
		return _result;
	}

	Vector2 CalculatePosition(float a)
	{
		Vector2 Point;

		if (a <= 90)
		{
			a *= Mathf.Deg2Rad;

			Point.x = Mathf.Cos(a) * Radius;
			Point.y = Mathf.Sin(a) * Radius;
		}
		else
			if (a <= 180)
			{
				a -= 90;

				a *= Mathf.Deg2Rad;
				Point.y = Mathf.Cos(a) * Radius;
				Point.x = -Mathf.Sin(a) * Radius;
			}
			else
				if (a <= 270)
				{
					a = a - 180;
					a *= Mathf.Deg2Rad;
					Point.x = -Mathf.Cos(a) * Radius;
					Point.y = -Mathf.Sin(a) * Radius;
				}
				else
				{
					a = a - 270;
					a *= Mathf.Deg2Rad;
					Point.y = -Mathf.Cos(a) * Radius;
					Point.x = Mathf.Sin(a) * Radius;
				}

		return Point;

	}


	IEnumerator Hop(Vector2 attemptPosition, Quaternion attemptRotation)
	{

		Vector3 startPos = transform.localPosition;
		Quaternion startRotation = transform.localRotation;

		float timer = 0.0f;
		float time = TimeToJump;

		while (timer <= 1f)
		{
			float height = Mathf.Sin(Mathf.PI * timer) * hopHeight;

			transform.localPosition = (Vector2.Lerp(startPos, attemptPosition, timer) + Vector2.up * height);

			if (timer == 1f)
			{
				transform.localRotation = attemptRotation;
				break;
			}

			if (timer < 1f)
			{
				transform.localRotation = Quaternion.Lerp(startRotation, attemptRotation, timer);
				timer += Time.deltaTime / time;
				if (timer > 1f)
					timer = 1f;
			}
			yield return null;
		}

		if(ArriveEvent != null)
			ArriveEvent(Character);

        yield break;
	}

    #endregion

    #region Rotate

    public void RotateRandom()
    {
        Vector2 v2 = new Vector2(Random.Range(-100f, 100f), Random.Range(-100f, 100f));
        RotateTo(v2 + (Vector2)transform.position);
    }

    public void RotateTo(Transform v2, bool SendMessage)
    {
        if (!MovementPermission)
            return;
        if (v2.position == transform.parent.position)
        {
            return;
            OverRotating();
        }
        Character.Body.ChangeWalkingState(true);
        
        StopAllCoroutines();

        Anchor = transform.parent;

        StartCoroutine(Rotate(v2));
    }

    public void RotateTo(Vector2 v2)
    {
        tmp.position = v2;
        RotateTo(tmp,true);
    }

    void OverRotating()
	{
		Character.Body.ChangeWalkingState(false);
        if (Message != null)
        {
            Message();
        }
    }

    float Distance(Vector2 Point1, Vector2 Point2)
    {
        return (Mathf.Abs(Point2.x - Point1.x) + Mathf.Abs(Point2.y - Point1.y));
    }

    int check(float A)
    {
        if ( -A > 0)
            return -1;
        else
            return 1;
    }

    int GetDirection(Vector2 A, Vector2 B, Vector2 C) {
        return -check((A.y - B.y) * C.x + (B.x - A.x) * C.y + (A.x* B.y) - (B.x* A.y));
    }

    float GetAngle(Vector2 Origin, Vector2 Point2, Vector2 Point3)
	{
        float _result = Vector2.Angle(Point2-Origin, Point3-Origin);

        int tmpDirection = GetDirection(Origin,Point2, Point3);

        if (tmpDirection != Direction)
            Flip();

        Direction = tmpDirection;

        return _result;
	}
    
	IEnumerator Rotate(Transform v2)
	{
		Angle = GetAngle(Anchor.position, v2.position, transform.position);

        int nr = 0;

        while (Angle >= 10) {
            nr++;

            if (Angle < AngleToRun)
            {
                transform.RotateAround(Anchor.position, Vector3.forward, min(WalkSpeed * Direction * Time.deltaTime, Angle));
                Angle -= min(WalkSpeed * Direction * Time.deltaTime, Angle);
            }
            else
            {
                transform.RotateAround(Anchor.position, Vector3.forward, min(RunSpeed * Direction * Time.deltaTime, Angle));
                Angle -= min(RunSpeed * Direction * Time.deltaTime, Angle);
            }

            yield return  null;

            if (nr>=15)
            {
                nr = 0;
                Angle =  GetAngle(Anchor.position, v2.position, transform.position);
            }

		}

        OverRotating();
       
		yield break;
	}

    public void Flip()
    {
        Debug.Log(transform.GetChild(0));
        Vector3 tmpScale = transform.GetChild(0).localScale;
        tmpScale.x *= -1;
        transform.GetChild(0).localScale = tmpScale;
    }

	#endregion

	float min(float a, float b) { return a < b ? a : b; }
	float max(float a, float b) { return a > b ? a : b; }


}