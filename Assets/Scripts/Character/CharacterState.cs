using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public enum CharacterStates
{
    Normal,
    Ice,
    Fire,
    Fired,
    Frozen,
    Fat
}

public class CharacterState
{
    public static IDictionary<CharacterStates, CharacterState> StateDex = new Dictionary<CharacterStates, CharacterState>()
    {
        { CharacterStates.Normal, new CharacterState("normal", CharacterStates.Normal) },
        { CharacterStates.Ice, new IceState("ice", CharacterStates.Ice) },
        { CharacterStates.Fire, new FireState("fire", CharacterStates.Fire) },
        { CharacterStates.Fired, new FiredState("fired", CharacterStates.Fired) },
        { CharacterStates.Frozen, new FrozenState("frozen", CharacterStates.Frozen) },
        { CharacterStates.Fat, new CharacterState("fat", CharacterStates.Fat) }
    };
   
    public string Name { get; internal set; }
    public CharacterStates Type { get; internal set; }

    public CharacterState(string name, CharacterStates type)
    {
        Name = name;
        Type = type;
    }

    public virtual void Close(Character Character)
    {

    }

    public virtual void Start(Character Character)
    {

    }

    private class FiredState : CharacterState
    {
        public FiredState(string name, CharacterStates type) : base(name, type)
        {
        }

        public override void Close(Character Character)
        {
            Character.PM.MovementPermission = true;
        }

        public override void Start(Character Character)
        {
            Character.PM.MovementPermission = false;
        }
    }

    private class FrozenState : CharacterState
    {
        public FrozenState(string name, CharacterStates type) : base(name, type)
        {
        }

        public override void Close(Character Character)
        {
            Character.PM.MovementPermission = true;
        }

        public override void Start(Character Character)
        {
            Character.PM.MovementPermission = false;
        }
    }

    private class FireState : CharacterState
    {
        public FireState(string name, CharacterStates type) : base(name, type)
        {
        }

        public override void Close(Character Character)
        {
            if (Character.PC.ActualPower.isActive)
                Character.PC.ActualPower.StopPow();
        }

        public override void Start(Character Character)
        {
        }
        
    }

    private class IceState : CharacterState
    {
        public IceState(string name, CharacterStates type) : base(name, type)
        {
        }

        public override void Close(Character Character)
        {
            if (Character.PC.ActualPower.isActive)
                Character.PC.ActualPower.StopPow();
        }

        public override void Start(Character Character)
        {
        }
        
    }
}