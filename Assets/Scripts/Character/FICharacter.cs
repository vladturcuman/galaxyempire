﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class FICharacter : Character, IFICharacter
{
    public virtual void Freez()
    {
        photonView.RPC("SetState", PhotonTargets.All, CharacterStates.Frozen);
    }

    public virtual void Fired()
    {
        photonView.RPC("SetState", PhotonTargets.All, CharacterStates.Fired);
    }
}
