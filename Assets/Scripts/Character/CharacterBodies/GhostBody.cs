using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;

class GhostBody : Photon.PunBehaviour, ICharacterBody
{
    
    const int eye1 = 7, eye2 = 11, eye3 = 15, body = 9;
    const int eye1Pupil = 8, eye2Pupil = 12, eye3Pupil = 16;
    const int eyeLBody = 13, eyeRBody = 14, eyeMiddleBody = 10;

    const int eyeL = 2, eyeR = 5, earL = 0, earR = 1, head = 4;
    const int eyeLPupil = 3, eyeRPupil = 6;

    const string path = "Characters/Ghost/";

    IDictionary<CharacterState, Sprite[]> Parts = new Dictionary<CharacterState, Sprite[]>();
    IDictionary<CharacterState, Sprite> Faces = new Dictionary<CharacterState, Sprite>();

    SpriteRenderer Eye1, Eye2, Eye3, Body;
    SpriteRenderer Eye1Pupil, Eye2Pupil, Eye3Pupil;
    SpriteRenderer EyeLBody, EyeRBody, EyeMiddleBody;

    SpriteRenderer EarL, EarR, EyeL, EyeR, Head;
    SpriteRenderer EyeLPupil, EyeRPupil;

    Animator anim;

    #region SetUp

    void Awake()
    {
        SetUpBody();
        Load();
    }

    public void SetCharacter(Character character)
    {
        if (Character != null)
            throw new InvalidOperationException();
        Character = character;
        Character.OnStateChanged += StateChanged;
    }

    void Load()
    {
        var type = typeof(CharacterState);
        foreach (CharacterState state in CharacterState.StateDex.Values)
        {
            try
            {
                Parts.Add(state, Resources.LoadAll<Sprite>(path + state.Name));
                Faces.Add(state, Resources.Load<Sprite>(path + state.Name + "Face"));

            }catch { }
        }
        anim = GetComponent<Animator>();
    }

    void SetUpBody()
    { 
        Body = transform.FindDeepChild("Body").GetComponent<SpriteRenderer>();
        Eye1 = transform.FindDeepChild("Eye1").GetComponent<SpriteRenderer>();
        Eye1Pupil = Eye1.transform.FindDeepChild("Pupil").GetComponent<SpriteRenderer>();
        Eye2 = transform.FindDeepChild("Eye2").GetComponent<SpriteRenderer>();
        Eye2Pupil = Eye2.transform.FindDeepChild("Pupil").GetComponent<SpriteRenderer>();
        Eye3 = transform.FindDeepChild("Eye3").GetComponent<SpriteRenderer>();
        Eye3Pupil = Eye3.transform.FindDeepChild("Pupil").GetComponent<SpriteRenderer>();
        EyeLBody = transform.FindDeepChild("EyeL").GetComponent<SpriteRenderer>();
        EyeRBody = transform.FindDeepChild("EyeR").GetComponent<SpriteRenderer>();
        EyeMiddleBody = transform.FindDeepChild("EyeMiddle").GetComponent<SpriteRenderer>();

        Head = transform.FindDeepChild("Head").GetComponent<SpriteRenderer>();
        EarL = Head.transform.FindDeepChild("EarL").GetComponent<SpriteRenderer>();
        EarR = Head.transform.FindDeepChild("EarR").GetComponent<SpriteRenderer>();
        EyeR = Head.transform.FindDeepChild("EyeR").GetComponent<SpriteRenderer>();
        EyeRPupil = EyeR.transform.FindDeepChild("Pupil").GetComponent<SpriteRenderer>();
        EyeL = Head.transform.FindDeepChild("EyeL").GetComponent<SpriteRenderer>();
        EyeLPupil = EyeL.transform.FindDeepChild("Pupil").GetComponent<SpriteRenderer>();
    }

    #endregion

    #region ICharacterBody
    Sprite face;
    Character Character;

    public Sprite Face { get { return face; } }

    void StateChanged(Character Character)
    {
        Sprite[] sp = Parts[Character.State];
        
        Body.sprite = sp[body];
        Eye1.sprite = sp[eye1];
        Eye1Pupil.sprite = sp[eye1Pupil];
        Eye2.sprite = sp[eye2];
        Eye2Pupil.sprite = sp[eye2Pupil];
        Eye3.sprite = sp[eye3];
        Eye3Pupil.sprite = sp[eye3Pupil];
        EyeLBody.sprite = sp[eyeLBody];
        EyeRBody.sprite = sp[eyeRBody];
        EyeMiddleBody.sprite = sp[eyeMiddleBody];

        EyeL.sprite = sp[eyeL];
        EyeLPupil.sprite = sp[eyeLPupil];
        EyeR.sprite = sp[eyeR];
        EyeRPupil.sprite = sp[eyeRPupil];
        EarR.sprite = sp[earR];
        EarL.sprite = sp[earL];
        Head.sprite = sp[head];

        face = Faces[Character.State];

        sp = null;
    }
    
    public void ChangeWalkingState(bool value)
    {
        anim.SetBool("isWalking", value);
    }

    #endregion

}

