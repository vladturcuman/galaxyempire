﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

class PMonsterBody : Photon.PunBehaviour, ICharacterBody
{
    const string path = "Characters/PMonster/";
    
    IDictionary<CharacterState, Sprite[]> Parts = new Dictionary<CharacterState, Sprite[]>();

    Animator anim;


    const int body=12, mouth=15;
    const int handR=6, clawUpR=9, clawMiddleR=10, clawDownR=11;
    const int handL=0, clawUpL=5, clawMiddleL=3, clawDownL=4;
    const int eyeR=7, pupilR=8, eyeL=1, pupilL=2, eye=13, pupil=14;

    SpriteRenderer Body, Mouth;
    SpriteRenderer HandR, ClawUpR, ClawMiddleR, ClawDownR;
    SpriteRenderer HandL, ClawUpL, ClawMiddleL, ClawDownL;
    SpriteRenderer EyeR, PupilR, EyeL, PupilL, Eye, Pupil;
    

    Character Character;

    void Awake()
    {
        Load();
        SetUpBody();
    }

    void Load()
    {
        var type = typeof(CharacterState);
        foreach (CharacterState state in CharacterState.StateDex.Values)
        {
            try
            {
               
                Parts.Add(state, Resources.LoadAll<Sprite>(path + state.Name));
            }
            catch { }
        }
        anim = GetComponent<Animator>();
    }

    void SetUpBody()
    {
        Body = transform.FindChild("body").GetComponent<SpriteRenderer>();
        Mouth = Body.transform.FindChild("Mouth").GetComponent<SpriteRenderer>();
        HandR = Body.transform.FindChild("HandR").GetComponent<SpriteRenderer>();
        HandL = Body.transform.FindChild("HandL").GetComponent<SpriteRenderer>();
        Eye = Body.transform.FindChild("Eye").GetComponent<SpriteRenderer>();

        ClawUpR = HandR.transform.FindChild("ClawUp").GetComponent<SpriteRenderer>();
        ClawMiddleR = HandR.transform.FindChild("ClawMiddle").GetComponent<SpriteRenderer>();
        ClawDownR = HandR.transform.FindChild("ClawDown").GetComponent<SpriteRenderer>();
        EyeR = HandR.transform.FindChild("Eye").GetComponent<SpriteRenderer>();

        ClawUpL = HandL.transform.FindChild("ClawUp").GetComponent<SpriteRenderer>();
        ClawMiddleL = HandL.transform.FindChild("ClawMiddle").GetComponent<SpriteRenderer>();
        ClawDownL = HandL.transform.FindChild("ClawDown").GetComponent<SpriteRenderer>();
        EyeL = HandR.transform.FindChild("Eye").GetComponent<SpriteRenderer>();

        Pupil = Eye.transform.FindChild("Pupil").GetComponent<SpriteRenderer>();
        PupilR = EyeR.transform.FindChild("Pupil").GetComponent<SpriteRenderer>();
        PupilL = EyeL.transform.FindChild("Pupil").GetComponent<SpriteRenderer>();
    }

    void StateChanged(Character character)
    {
        Sprite[] sp = Parts[character.State];
        if (sp == null)
            sp = Parts[CharacterState.StateDex[CharacterStates.Normal]];

        Body.sprite = sp[body];
        
        Eye.sprite = sp[eye];
        Pupil.sprite = sp[pupil];
        EyeR.sprite = sp[eyeR];
        PupilR.sprite = sp[pupilR];
        EyeL.sprite = sp[eyeL];
        PupilL.sprite = sp[pupilL];
        Mouth.sprite = sp[mouth];

        HandL.sprite = sp[handL];
        ClawDownL.sprite = sp[clawDownL];
        ClawMiddleL.sprite = sp[clawDownL];
        ClawMiddleL.sprite = sp[clawMiddleL];

        HandR.sprite = sp[handR];
        ClawDownR.sprite = sp[clawDownR];
        ClawMiddleR.sprite = sp[clawDownR];
        ClawMiddleR.sprite = sp[clawMiddleR];

        sp = null;
    }

    public void ChangeWalkingState(bool value)
    {
        anim.SetBool("isWalking", value);
        if(value)
        {
            anim.Play("start_moving", -1, 0f);
        }
    }

    public void SetCharacter(Character character)
    {
        if (Character != null)
            throw new InvalidOperationException();
        Character = character;
        Character.OnStateChanged += StateChanged;
    }
}