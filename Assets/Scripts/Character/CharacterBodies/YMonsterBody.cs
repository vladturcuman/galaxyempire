﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

class YMonsterBody : Photon.PunBehaviour, ICharacterBody
{
    const string path = "Characters/YMonster/";
    
    IDictionary<CharacterState, Sprite[]> Parts = new Dictionary<CharacterState, Sprite[]>();

    Animator anim;


    const int body=10, mouth=9, tongue=4;
    const int handR=0, handL=1, legR=2, legL=3;
    const int eye=8, eyeL=5, eyeM=6, eyeR=7;

    SpriteRenderer Body, Mouth, Tongue;
    SpriteRenderer HandR, HandL,LegR,LegL;
    SpriteRenderer Eye, EyeL, EyeM, EyeR;

    Character Character;

    void Awake()
    {
        Load();
        SetUpBody();
    }

    void Load()
    {
        var type = typeof(CharacterState);
        foreach (CharacterState state in CharacterState.StateDex.Values)
        {
            try
            {
                Parts.Add(state, Resources.LoadAll<Sprite>(path + state.Name));
            }
            catch { }
        }
        anim = GetComponent<Animator>();
    }

    void SetUpBody()
    {
        GameObject Body = transform.FindChild("body").gameObject;
        Mouth = Body.transform.FindChild("Mouth").GetComponent<SpriteRenderer>();
        Tongue = Mouth.transform.GetChild(0).GetComponent<SpriteRenderer>();

        HandR = Body.transform.FindChild("HandR").GetComponent<SpriteRenderer>();
        HandL= Body.transform.FindChild("HandL").GetComponent<SpriteRenderer>();
        LegL = Body.transform.FindChild("LegL").GetComponent<SpriteRenderer>();
        LegR = Body.transform.FindChild("LegR").GetComponent<SpriteRenderer>();

        Eye = Body.transform.FindChild("Eye").GetComponent<SpriteRenderer>();
        EyeR = Body.transform.FindChild("EyeR").GetComponent<SpriteRenderer>();
        EyeM = Body.transform.FindChild("EyeM").GetComponent<SpriteRenderer>();
        EyeL = Body.transform.FindChild("EyeL").GetComponent<SpriteRenderer>();

        this.Body = Body.transform.FindChild("body").GetComponent<SpriteRenderer>();
    }

    void StateChanged(Character character)
    {
        if (character.State.Type == CharacterStates.Fat)
        {
            if (transform.localScale.x > 0)
                transform.localScale = new Vector3(.25f, .2f);
            else
                transform.localScale = new Vector3(-0.25f, .2f);

            return;
        }
        else
        {
            if (transform.localScale.x == .25f)
                transform.localScale = new Vector3(.2f, .2f);
            if (transform.localScale.x == -0.25f)
                transform.localScale = new Vector3(-0.2f, .2f);
        }

        Sprite[] sp = Parts[character.State];
        if (sp == null)
            sp = Parts[CharacterState.StateDex[CharacterStates.Normal]];

        Body.sprite = sp[body];
        
        Mouth.sprite = sp[mouth];
        Tongue.sprite = sp[tongue];

        HandR.sprite = sp[handR];
        HandL.sprite = sp[handL];

        LegL.sprite = sp[legL];
        LegR.sprite = sp[legR];

        Eye.sprite = sp[eye];
        EyeL.sprite = sp[eyeL];
        EyeM.sprite = sp[eyeM];
        EyeR.sprite = sp[eyeR];
        sp = null;
    }

    public void ChangeWalkingState(bool value)
    {
        anim.SetBool("isWalking", value);
    }

    public void SetCharacter(Character character)
    {
        if (Character != null)
            throw new InvalidOperationException();
        Character = character;
        Character.OnStateChanged += StateChanged;
    }
}