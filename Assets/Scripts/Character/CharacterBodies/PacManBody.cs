﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;

class PacManBody : Photon.PunBehaviour, ICharacterBody
{
    const string path = "Characters/PacMan/";

    Animator anim;

    void Awake()
    {
        anim = GetComponent<Animator>();
        face = Resources.Load<Sprite>(path + "body");
    }

    Sprite face;
    Character Character;

    public Sprite Face { get { return face; } }
    
    void StateChanged(Character Character)
    {
    }

    public void ChangeWalkingState(bool value)
    {
    }

    public void SetCharacter(Character Character)
    {
        //throw new NotImplementedException();
    }

}
