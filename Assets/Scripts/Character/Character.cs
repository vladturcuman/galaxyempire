using System;
using UnityEngine;
using System.Collections.Generic;


using UnityEngine.UI;

public class Character : Photon.PunBehaviour
{
    #region Character

    public static Vector3 StartPosition = new Vector3(100, 100);

    public PowersController PC;
	public CharacterMover PM;
    public ICharacterBody Body;
    public CharacterTypes Type;

    public delegate void Action(Character p);
    public static event Action OnAwaked;

    public event Action<Character> OnStateChanged;
    public event Action<Character> OnManualControlChanged;

    int id;
    bool isManualControlled;
    CharacterState state;
    
    public int ID { get { return id; } }
    public CharacterState State { get { return state; } }
    public bool IsManualControlled
    {
        get { return isManualControlled; }
        set
        {
            if(isManualControlled != value)
            {
                isManualControlled = value;
                if(OnManualControlChanged != null)
                    OnManualControlChanged(this);
            }
        }
    }

    internal virtual void Awake()
    {
        state = CharacterState.StateDex[CharacterStates.Normal];

        Body = GetComponentInChildren<ICharacterBody>();
        Body.SetCharacter(this);

        id = GameController.Characters.Count;
        GameController.Characters.Add(this);

        if (GameController.rescaler != null)
            GameController.rescaler.AddToList(transform);

        if (OnAwaked != null)
            OnAwaked(this);
    }

    void Start()
    {
        Normalize();
    }

    void OnDestroy()
    {
        OnAwaked = null;
        GameController.Characters.Remove(this);
    }

    public virtual void CreatePM()
    {
        PM = gameObject.AddComponent<CharacterMover>();
        PM.SetCharacter(this);
    }

    public void CreatePC()
    {
        PC = gameObject.AddComponent<PowersController>();
        PC.Character = this;
    }

    [PunRPC]
    internal void SetState(CharacterStates state)
    {
        if (!photonView.isMine)
            this.state = CharacterState.StateDex[state];
        else
        {
            CharacterState oldState = this.state;
            this.state = CharacterState.StateDex[state];
            oldState.Close(this);
            oldState = null;
            this.state.Start(this);
        }

        if (OnStateChanged != null)
            OnStateChanged(this);
    }
    
    public void ChangeParent(IStation Planet)
    {
        photonView.RPC("ChangeParent", PhotonTargets.All, Planet.ID);
    }

    [PunRPC]
    public void ChangeParent(int id)
    {
        if (id == -1)
            gameObject.transform.parent = null;
        else
            gameObject.transform.SetParent(GameController.Stations[id].Transform, true);
    }
    
    [PunRPC]
    public void SetName(string name)
    {
        if (photonView.isMine)
            photonView.RPC("SetName", PhotonTargets.OthersBuffered, name);

        GetComponentInChildren<Text>().text = name;
    }

    public string GetName()
    {
        return GetComponentInChildren<Text>().text;
    }

    public virtual bool CanStayOn(IStation station)
    {
        if (station is IFIPlanet)
            return (State.Type == CharacterStates.Normal && (station.State == PlanetStates.Fired || station.State == PlanetStates.Frozen));
        else
            return true;
    }

    public IStation GetStation()
    {
        if (PM != null)
            return PM.PCurrentStation;
        else
            if (transform.parent != null)
            return transform.parent.GetComponent<IStation>();
        else
            return null;
    }
    
    public void Arrive(IStation from, IStation to)
    {
        if(to!=null)
            to.CharacterArrive(this);
    }

    public void Leave(IStation station)
    {
        if(station != null)
            station.CharacterLeave(this);
    }
    
    public virtual void Normalize()
    {
        if (PhotonNetwork.inRoom)
            photonView.RPC("SetState", PhotonTargets.All, CharacterStates.Normal);
        else
            SetState(CharacterStates.Normal);
    }
    #endregion
}

public enum CharacterTypes
{
    [CharacterTypesValue(0)]
    PMonster,
    [CharacterTypesValue(60)]
    Ghost,
    [CharacterTypesValue(0)]
    YMonster
}

public class CharacterTypesValue : System.Attribute
{
    public int Price { get; set; }

    public CharacterTypesValue(int price)
    {
        Price = price;
    }
}