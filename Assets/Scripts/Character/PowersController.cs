using UnityEngine;
using System.Collections;
using System.Reflection;
using System.Collections.Generic;


using UnityEngine.UI;
using System;

public class PowersController : Photon.PunBehaviour {

    public Character Character;
    public IList<PowerTypes> PowersQue = new List<PowerTypes>();

    public IPower ActualPower = null;

    public delegate void Action();
    public event Action PowersChanged;

    void Awake()
    {

    }
    
    public void AddPowToQue(PowerTypes newPow)
    {
        if (ActualPower == null)
            ChangePower(newPow);
        else
        {
            PowersQue.Add(newPow);
            if (PowersChanged != null)
                PowersChanged();
        }
    }

    void PowerStopped(IPower power)
    {
        ActualPower.OnStoped -= PowerStopped;
        ActualPower = null;

        UseNextPow();
    }

    public void UseNextPow()
    {
        if (ActualPower != null)
        {
            ActualPower.OnStoped -= PowerStopped;
            ActualPower.StopPow();
            ActualPower = null;
        }

        if (PowersQue.Count > 0)
        {
            ActualPower = (IPower)gameObject.AddComponent(PowersManager.IconDex[PowersQue[0]].Type);
            PowersQue.RemoveAt(0);
            ActualPower.OnStoped += PowerStopped;
            ActualPower.StartPow(Character);
        }

		if(PowersChanged != null)
			PowersChanged ();
    }

    public void ChangePower(PowerTypes newPow)
    {
        if (ActualPower != null)
        {
            ActualPower.OnStoped -= PowerStopped;
            ActualPower.StopPow();
            ActualPower = null;
        }

        ActualPower = (IPower)gameObject.AddComponent(PowersManager.IconDex[newPow].Type);
        ActualPower.OnStoped += PowerStopped;
        ActualPower.StartPow(Character);

        if (PowersChanged != null)
            PowersChanged();
    }
}

