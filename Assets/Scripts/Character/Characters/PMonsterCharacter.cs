﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

class PMonsterCharacter : FICharacter
{
    public override void CreatePM()
    {
        base.CreatePM();

        PM.NormalRunSpeed = PM.RunSpeed = 60f;
        PM.NormalWalkSpeed = PM.WalkSpeed = 40f;
    }
    
}
