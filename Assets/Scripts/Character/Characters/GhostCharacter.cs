﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

class GhostCharacter : FICharacter, IFatable
{
    public override void CreatePM()
    {
        base.CreatePM();
        PM.NormalRunSpeed = PM.RunSpeed = 130f;
        PM.NormalWalkSpeed = PM.WalkSpeed = 40f;
    }

    public void GetFat()
    {
        PM.RunSpeed = 40;
        PM.WalkSpeed = 30;
        photonView.RPC("SetState", PhotonTargets.All, CharacterStates.Fat);
    }

    public override void Normalize()
    {
        if(State.Type == CharacterStates.Fat)
        {
            PM.RunSpeed = 130;
            PM.WalkSpeed = 40;
        }
        base.Normalize();
    }
}
