﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

class YMonsterCharacter : FICharacter, IFatable
{
    public override void CreatePM()
    {
        base.CreatePM();
        PM.NormalRunSpeed = PM.RunSpeed = 60f;
        PM.NormalWalkSpeed = PM.WalkSpeed = 40f;
    }

    public void GetFat()
    {
        PM.RunSpeed = 30;
        PM.WalkSpeed = 20;
        photonView.RPC("SetState", PhotonTargets.All, CharacterStates.Fat);
    }

    public override void Normalize()
    {
        if(State.Type == CharacterStates.Fat)
        {
            PM.RunSpeed = 60;
            PM.WalkSpeed = 40;
        }
        base.Normalize();
    }

    public override void Fired()
    {

    }

    public override void Freez()
    {

    }

    public override bool CanStayOn(IStation station)
    {
        return true;
    }
}
