﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

class PacmanCharacter : FICharacter
{
    public override void CreatePM()
    {
        base.CreatePM();

        PM.NormalRunSpeed = PM.RunSpeed = 40f;
        PM.NormalWalkSpeed = PM.WalkSpeed = 20f;
    }
}