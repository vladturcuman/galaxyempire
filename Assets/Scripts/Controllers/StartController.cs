using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;


class StartController : Photon.MonoBehaviour
{
    #region Variables
    CharacterTypes startCharacterType = CharacterTypes.PMonster;
    Vector3 scaleOffset = new Vector3(0.3f, 0.3f, 0);
    public IStation ShopStation, RepositoryStation, StandingStation;

    /// Ui Elements
    public GameObject settingsButton;
    public GameObject noWifiButton;
    public GameObject waitMenu;

    public InputField NameInputField;
    public InputField SettingsInputField;

    public Text pointsText;
    public Text timerField;

    /// World Elements
    public Planet bigPlanet;
    public Planet settingsPlanet;

    /// Variables
        // Movement Variables
        Vector2 firstMenuPoint;
        Vector2 secondMenuPoint;
        Vector2 thirdMenuPoint;
        Vector2 settingsMenuPoint;
        Vector2 characterMenuPoint;
        Vector2 NamePoint;

        GameObject currentPlanet;
        Transform cameraTransform;

        int actualMenu;
        int Direction;
        int Speed = 120;
        int LowSpeed = 30;
        static int points;

        float timer;
        float Angle;
        float time = 0.3f;

        const float NormalWalkSpeed = 50;
        const float NormalRunSpeed = 70;

        Transform tmp;
    // Character Info Variables

    public static event Action<int, int> OnPointsChanged;
    public static int Points
    {
        get { return points; }
        set
        {
            if(OnPointsChanged != null)
                OnPointsChanged(points, value);
            points = value;
            PlayerPrefs.SetInt("Points", value);
        }
    }
        public static string name;

        // Static Variables
        public static StartController startController;
        public static Character MyCharacter;

        // Room Timer Variables
        int roomTimer;
        int lastCharacterCount;
        const int startRoomTimer = 10;    

    #endregion

    void Start()
    {
        startController = this;
        tmp = (new GameObject()).GetComponent<Transform>();
        cameraTransform = Camera.main.transform;
        LoadData();
        OnPointsChanged += RefreshPointsField;

        NetworkController.FailMessage += Fail;
        NetworkController.OkMessage += Ok;

        if (!PhotonNetwork.connectedAndReady)
            Reconnect();
        else
            Ok();
    }

    void RefreshPointsField(int OldValue, int newValue)
    {
        if (newValue == OldValue)
            return;

        string message = (newValue - OldValue).ToString();

        if (newValue > OldValue)
        {
            UIController.LogMessage("+" + message, UIController.Green);
        }
        else
            UIController.LogMessage(message, UIController.Red);
        pointsText.text = newValue.ToString();
    }

    public void SetData(bool first)
    {
        if (first)
        {
            PlayerPrefs.SetString("name", NameInputField.text);
            PlayerPrefs.SetInt("points", 0);
            PlayerPrefs.SetString("CharacterType", startCharacterType.ToString());
            PlayerPrefs.SetInt(startCharacterType.ToString(), 0);
        }
        else
        {
            PlayerPrefs.SetString("name", SettingsInputField.text);
            MyInfo.name =  name = SettingsInputField.text;
            MyCharacter.SetName(name);
        }

        PlayerPrefs.Save();

        if (first)
            LoadData();
    }

    void LoadData()
    {
        // Load Movement Variables
        if(bigPlanet == null)
        {
            bigPlanet = GameObject.Find("bigPlanet").GetComponentInChildren<Planet>();
            settingsPlanet = GameObject.Find("settingsPlanet").GetComponentInChildren<Planet>();
            ShopStation = GameObject.Find("ShopPlanet").GetComponentInChildren<IStation>();
            RepositoryStation = GameObject.Find("RepositoryPlanet").GetComponentInChildren<IStation>();
            StandingStation = GameObject.Find("StandingPlanet").GetComponentInChildren<IStation>();

            firstMenuPoint = GameObject.Find("firstMenuPoint").transform.position;
            thirdMenuPoint = GameObject.Find("thirdMenuPoint").transform.position;
            secondMenuPoint = GameObject.Find("secondMenuPoint").transform.position;
            settingsMenuPoint = GameObject.Find("settingsMenuPoint").transform.position;
            characterMenuPoint = GameObject.Find("characterMenuPoint").transform.position;
            NamePoint = GameObject.Find("NamePoint").transform.position;
        }

        // Load CharacterInfo
        if (PlayerPrefs.HasKey("name"))
        {
            if (points == 0)
            {
                name = PlayerPrefs.GetString("name");
                points = PlayerPrefs.GetInt("points");
            }

            MyInfo.name = name;

            CreateMyCharacter();
            CreateTheOtherCharacters();
            MyCharacter.PM.SetOn(bigPlanet);
            MyCharacter.PM.Message += CharacterArrive;
            
            pointsText.text = points.ToString();

            currentPlanet = bigPlanet.gameObject;

            Camera.main.transform.position = firstMenuPoint;

            settingsButton.SetActive(true);

            currentPlanet = (bigPlanet).gameObject;

            actualMenu = 1;

        }
        else
            Camera.main.transform.position = NamePoint;
    }

    void CreateMyCharacter()
    {
        MyCharacter = ((GameObject)Instantiate(Resources.Load<GameObject>("Characters/" + PlayerPrefs.GetString("CharacterType") + "/Character"), Vector3.zero, Quaternion.identity)).GetComponent<Character>();
        MyCharacter.transform.FindChild("PlayerBody").localScale += new Vector3(0.1f, 0.1f);
        SetUpMyCharacter();
    }

    public void SetUpMyCharacter()
    {
        MyCharacter.CreatePM();
        MyCharacter.SetName(MyInfo.name);
        MyCharacter.IsManualControlled = true;
        MyCharacter.Type = (CharacterTypes)Enum.Parse(typeof(CharacterTypes), PlayerPrefs.GetString("CharacterType"));
    }

    void CreateTheOtherCharacters()
    {
        var type = typeof(CharacterTypes);
        foreach (CharacterTypes state in Enum.GetValues(type))
            if(state.ToString() != PlayerPrefs.GetString("CharacterType"))
            {
                Character character = ((GameObject)Instantiate(Resources.Load<GameObject>("Characters/" + state + "/character"), Vector3.zero, Quaternion.identity)).GetComponent<Character>();
                character.transform.localScale -= scaleOffset;
                character.transform.FindChild("PlayerBody").localScale += new Vector3(0.1f, 0.1f);
                character.CreatePM();
                character.IsManualControlled = true;
                character.Type = state;

                if (PlayerPrefs.HasKey(state.ToString()))
                {
                    character.SetName(state.ToString());
                    character.PM.GoTo(RepositoryStation);
                }
                else
                {
                    character.PM.GoTo(ShopStation);
                    character.SetName(((CharacterTypesValue)(type.GetMember(state.ToString())[0].GetCustomAttributes(true)[0])).Price.ToString());
                }

                character.PM.OnPArrive += RotateCharacterRandom;
            }
    }

    void RotateCharacterRandom(IStation from, IStation to)
    {
        foreach(Character character in to.Visitors)
        {
            character.PM.RotateRandom();
        }
    }

    #region RoomControl

    public void InRoom(bool value)
    {
        if (value)
            GoTo(3);
        else
            GoTo(2);
        DeleteJunk();
        waitMenu.SetActive(value);
        if (value)
            InvokeRepeating("RoomTimer", 0f, 1f);
        else
            CancelInvoke("RoomTimer");
    }

    void DeleteJunk()
    {
        roomTimer = startRoomTimer;
        timerField.text = "";
        lastCharacterCount = 0;
        waitMenu.SetActive(true);

    }

    void RoomTimer()
    {
        try
        {
            if (PhotonNetwork.room.playerCount != lastCharacterCount)
                DeleteJunk();

            if (PhotonNetwork.room.playerCount >= 1)
            {
                roomTimer--;
                timerField.text = roomTimer.ToString();
                lastCharacterCount = PhotonNetwork.room.playerCount;

                if (roomTimer <= 3)
                    waitMenu.SetActive(false);
                if (roomTimer <= 0)
                    NetworkController.networkcontroller.photonView.RPC("LoadGame", PhotonTargets.All);
            }
            else
                DeleteJunk();

        }
        catch
        {
            DeleteJunk();
        }

    }
    
    #endregion

    #region NetworkControl

    public void Reconnect()
    {
        NetworkController.Connect();
    }

    public void Fail()
    {
        noWifiButton.SetActive(true);
        ShowPopUp("Connection failed.", "Menu/Buttons/NoWifi");
    }

    public void Ok()
    {
        noWifiButton.SetActive(false);
    }

    void ShowPopUp(string message, string path)
    {
        PopUpManager.popupmanager.SetUp(message, path);
    }

    #endregion

    #region MovementFunctions

    public void GoTo(int index)
    {
        if (index == actualMenu)
            return;

        if (index != 3 && (NetworkController.tryToEnter==true || NetworkController.networkcontroller.amInRoom == true))
        {
            NetworkController.tryToEnter = false;
            NetworkController.networkcontroller.LeaveRoom();
        }
         
        settingsButton.SetActive(index==1);
        

        if(GetRotateNecesityForMenu(actualMenu,index))
        {
            Vector2 to = GetPositionForMenu(index);
            
            tmp.position = to;
            OverrideSpeed(false);

            if(MyCharacter.PM.HaveToGoSomewhere)
                MyCharacter.PM.OnPArrive += OnCharacterArrive;
            else
                MyCharacter.PM.RotateTo(tmp,true);

            RotateCamera(to);

            actualMenu = index;
            
            return;
        }

        MyCharacter.PM.DeleteJunk();

        currentPlanet = (index == 5 ? StandingStation : (index == 4 ? settingsPlanet : bigPlanet)).Transform.gameObject;

        if (actualMenu != 0)
            MyCharacter.PM.GoTo(index == 5 ? StandingStation : (index == 4 ? settingsPlanet : bigPlanet));

        TranslateCameraTo(GetPositionForMenu(index));

        actualMenu = index;
    }

    Vector2 GetPositionForMenu(int index)
    {
        switch(index)
        {
            case 1:
                return firstMenuPoint;
                
            case 2:
                return secondMenuPoint;
                
            case 3:
                return thirdMenuPoint;
                
            case 4:
                return settingsMenuPoint;

            case 5:
                return characterMenuPoint;
                
        }
        throw new System.Exception("StartController GetPositionForMenu indx out of range");
        
    }

    bool GetRotateNecesityForMenu(int from,int to)
    {
        if (from == 4 || to == 4)
            return false;
        if (from == 0)
            return false;
        return true;
    }

    public void TranslateCameraTo(Vector2 to)
    {
        StopAllCoroutines();
        StartCoroutine(TranslateTo(to));
    }
    
    IEnumerator TranslateTo(Vector3 v2)
    {
        v2.z = -1f;
        timer = 0;

        Vector3 StartPosition = Camera.main.transform.position;

        while (timer <= 1f)
        {
            Camera.main.transform.position = Vector3.Lerp(StartPosition, v2, timer);
            Camera.main.transform.rotation = Quaternion.Lerp(Camera.main.transform.rotation, Quaternion.identity, timer);

            if (timer == 1f)
            {
                yield break;
            }

            timer += Time.deltaTime / time;
            if (timer > 1f)
                timer = 1f;

            yield return null;
        }


        yield break;
    }

    public void RotateCamera(Vector2 toPos)
    {
        StopAllCoroutines();
        StartCoroutine(Rotate(toPos));
    }

    private float GetAngle(Vector2 Origin, Vector2 Point2, Vector2 Point3)
    {
        float angle1 = Mathf.Atan2(Origin.y - Point3.y, Origin.x - Point3.x);
        float angle2 = Mathf.Atan2(Origin.y - Point2.y, Origin.x - Point2.x);
        float _result = ((float)(angle2 - angle1)) * 180 / 3.14f;
        if (_result < 0)
        {
            _result += 360;
        }

        if (_result > 180)
        {

            Direction = -1;
            _result = 360 - _result;

            return _result;
        }
        Direction = 1;

        return _result;
    }

    IEnumerator Rotate(Vector2 v2)
    {


        Angle = GetAngle(currentPlanet.transform.position, v2, (Vector2)cameraTransform.position);

        while (Angle >= 0.5f)
        {
            if (Angle >= 5f)
                cameraTransform.RotateAround(currentPlanet.transform.position, Vector3.forward, Speed * Direction * Time.deltaTime);
            else
                cameraTransform.RotateAround(currentPlanet.transform.position, Vector3.forward, LowSpeed * Direction * Time.deltaTime);

            yield return null;

            Angle = GetAngle(currentPlanet.transform.position, v2, (Vector2)cameraTransform.position);


        }


        yield break;
    }

    public void CharacterArrive()
    {
        OverrideSpeed(true);
    }

    void OverrideSpeed(bool lessThanNow)
    {
        if(lessThanNow)
        {
            MyCharacter.PM.RunSpeed = NormalWalkSpeed;
        }
        else
        {
            MyCharacter.PM.RunSpeed = NormalRunSpeed;
        }
    }

    public void OnCharacterArrive(IStation From,IStation To)
    {
        MyCharacter.PM.OnPArrive -= OnCharacterArrive;
        if(To == StandingStation)
        {
            OverrideSpeed(true);
            MyCharacter.transform.localScale -= scaleOffset;
            return;
        }
        
        MyCharacter.PM.RotateTo(tmp,true);
    }
    #endregion

    #region SettingsFunction

    public void ActiveCharacterMenu(bool activate)
    {
        if (!activate)
        {
            MyCharacter.transform.localScale += scaleOffset;
            MyCharacter.PM.OnPArrive -= RotateCharacterRandom;
            OverrideSpeed(true);

            InputManager.CharacterEvent.OnTap -= OnTapCharacter;
        }
        else
        {
            InputManager.CharacterEvent.OnTap += OnTapCharacter;
            MyCharacter.PM.OnPArrive += RotateCharacterRandom;
            OverrideSpeed(false);
        }
        GoTo(activate ? 5 : 4);
        MyCharacter.PM.OnPArrive += OnCharacterArrive;
    }

    public void OnTapCharacter(GameObject go)
    {
        Debug.Log(go);
        Character Character = go.GetComponent<Character>();
        
        if (Character == MyCharacter || MyCharacter.PM.PCurrentStation != StandingStation)
            return;

        if (!PlayerPrefs.HasKey(Character.Type.ToString()))
        {
            int price = int.Parse(Character.GetName());
            if (points < price)
            {
                ShowPopUp("You don`t have enought money!", "Money/icon");
                return;
            }
            else
            {
                Points -= price;
                Character.SetName(Character.Type.ToString());
                PlayerPrefs.SetInt(Character.Type.ToString(), 0);
            }
        }   
        
        MyCharacter.PM.GoTo(RepositoryStation);
        Character.PM.GoTo(StandingStation);


        Character.SetName(MyCharacter.GetName());
        MyCharacter.SetName(MyCharacter.Type.ToString());

        MyCharacter = Character;
        PlayerPrefs.SetString("CharacterType", MyCharacter.Type.ToString());

    }

    #endregion

    void OnApplicationPause(bool pauseStatus)
    {
        if (pauseStatus == true)
        {
            PhotonNetwork.LeaveRoom();
        }
    }
    
    void OnDestroy()
    {
        GameController.Characters.Clear();
        GameController.Stations.Clear();
        MyCharacter = null;
        OnPointsChanged -= RefreshPointsField;
    }
}

