using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class NetworkController : Photon.PunBehaviour
{
    
	public static NetworkController networkcontroller;

    public static event System.Action FailMessage;
    public static event System.Action OkMessage;

    public static bool tryToEnter;

    public Character PhotonCharacter;
    public Character LocalCharacter;

    public bool amInRoom;


	void Start()
	{
		tryToEnter = false;
		networkcontroller = this;
	}

    #region Connect To Photon
    public static void Connect()
    {
		if (PhotonNetwork.connected) {
			if (OkMessage != null)
				OkMessage ();
			return;
		}
        PhotonNetwork.sendRate = 100;
        PhotonNetwork.sendRateOnSerialize = 100;
		try{ PhotonNetwork.ConnectUsingSettings("v1.0.1");
		}catch{
		}
    }

    public override void OnConnectedToMaster()
    {
        PhotonNetwork.JoinLobby();
    }

    public override void OnJoinedLobby()
    {
		if (OkMessage != null)
			OkMessage ();
    }

    public override void OnDisconnectedFromPhoton()
    {
        if(amInRoom)
        {
            LeaveRoom();
        }
        tryToEnter = false;
        if (FailMessage != null)
            FailMessage();
    }
    #endregion

    #region Connect To Room

    public void Enter()
    {
        if (!tryToEnter)
        {
            LocalCharacter = GameController.MyCharacter;
            tryToEnter = true;
            PhotonNetwork.JoinRandomRoom();
        }
    }

	public void OnPhotonRandomJoinFailed() 
	{
		string name = GetRandomName ();
		RoomOptions roomOptions = new RoomOptions() { isVisible = true, maxPlayers = 4 };
		try{
			PhotonNetwork.JoinOrCreateRoom(name, roomOptions, TypedLobby.Default);
		}catch{
			tryToEnter = false;
			if (FailMessage != null)
				FailMessage ();
		}
	}

	public override void OnJoinedRoom()
	{
        if (!tryToEnter)
        {
            amInRoom = false;
            tryToEnter = false;
            PhotonNetwork.LeaveRoom();
            return;
        }

        amInRoom = true;

        StartController.startController.InRoom (true);

        LocalCharacter = StartController.MyCharacter;
        LocalCharacter.gameObject.SetActive(false);

        StartController.MyCharacter = (PhotonNetwork.Instantiate(("Characters/" + PlayerPrefs.GetString("CharacterType") + "/Character"), Vector3.zero, Quaternion.identity,0)).GetComponent<Character>();
        StartController.startController.SetUpMyCharacter();

        StartController.MyCharacter.photonView.RPC("ChangeParent", PhotonTargets.AllBuffered, StartController.startController.bigPlanet.ID);
		StartController.MyCharacter.PM.RunSpeed = StartController.MyCharacter.PM.NormalWalkSpeed;

        PhotonCharacter = StartController.MyCharacter;

        Invoke ("DropCharacter", 1.3f);
	}

	public void LeaveRoom()
	{
        if (amInRoom)
        {
            amInRoom = false;

            PhotonNetwork.LeaveRoom();

            if (PhotonCharacter != null)
                Destroy(PhotonCharacter);
            
            StartController.MyCharacter = LocalCharacter;
            LocalCharacter.gameObject.SetActive(true);
            StartController.startController.InRoom(false);
        }
    }

	public override void OnLeftRoom ()
	{
		LeaveRoom ();
	}

    #endregion


    private string GetRandomName()
    {
        int name = StartController.name.GetHashCode();

        name += Time.timeSinceLevelLoad.GetHashCode() + Time.unscaledTime.GetHashCode();
        return name.ToString();
    }

    void DropCharacter()
	{
		if (PhotonNetwork.inRoom == false || amInRoom == false)
			return;

        PhotonCharacter.transform.position = new Vector3(-25f,-15f);

		StartController.MyCharacter.PM.TimeToJump = 0.9f;
		StartController.MyCharacter.PM.GoTo (StartController.startController.bigPlanet);

		Invoke ("CheckCharacterCount", 2f);
	}
    
    
	void CheckCharacterCount()
	{
		try{
			if (PhotonNetwork.room.playerCount == 4) {
				photonView.RPC ("LoadGame", PhotonTargets.All);
			}
		}catch{
		}
	}

    [PunRPC]void LoadGame()
    {
		//StartController.startController.SaveCharacterData ();
			PhotonNetwork.room.open = false;
            PhotonNetwork.room.visible = false;
            PhotonNetwork.room.maxPlayers = PhotonNetwork.room.playerCount;


        amInRoom = false;
		PhotonNetwork.LoadLevel("Game");
    }
 
	void OnDestroy()
	{
		OkMessage = null;
		FailMessage = null;
	}
}
