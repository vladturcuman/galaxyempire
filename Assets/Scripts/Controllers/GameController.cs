﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Reflection;
using System.Linq;
using UnityEngine.SceneManagement;
using System;

public class GameController : Photon.PunBehaviour
{
    public static bool AmInGame;

    public static int StartMoney = 40;
 
    public static Character MyCharacter;
    
    public static IDictionary<int, IStation> Stations = new Dictionary<int, IStation>();
    
    public static IList<Character> Characters = new List<Character>();

	public static GameController gameC;
	public static Transform MapTransform;

	public static Rescaler rescaler;

    List<Planet> LightedPlantes = new List<Planet>();

    void Awake()
    {
        gameC= this;
		rescaler = GetComponent<Rescaler> ();
        rescaler.enabled = true;
		MapTransform = GameObject.Find ("Map").transform;
        SetUp();

        if (PhotonNetwork.isMasterClient)
			Invoke("CreateMap", .1f);
    }

    void SetUp()
    {
        InputManager.PlanetEvent.OnTap += GoToPlanet;
        InputManager.PlanetEvent.OnTap += ShowPath;

        PowersManager.SetUp();
        AIManager.SetUp();
        Planet.SetUp();
        UIController.uiController.SetUp();
    }

    void CreateMap()
    {
        var type = typeof(StationTypes);
        foreach (StationTypes state in System.Enum.GetValues(type))
        {
            int numberOfStations = ((StationTypeValue)(type.GetMember(state.ToString())[0].GetCustomAttributes(true)[0])).Number;
            string path = ((StationTypeValue)(type.GetMember(state.ToString())[0].GetCustomAttributes(true)[0])).Path;

            while (numberOfStations > 0)
            {
                numberOfStations--;
                Vector2 position;
                do
                {
                    position = new Vector2(UnityEngine.Random.Range(SunPlanet.SunPosition.x - GravityController.MaxDistance, SunPlanet.SunPosition.x + GravityController.MaxDistance),
                        UnityEngine.Random.Range(SunPlanet.SunPosition.y - GravityController.MaxDistance, SunPlanet.SunPosition.y + GravityController.MaxDistance));
                } while (position.x - SunPlanet.SunPosition.x < 5 && position.y - SunPlanet.SunPosition.y < 5);
                
                PhotonNetwork.Instantiate(path, position, Quaternion.identity, 0);
            }
        }
        Invoke("ContinueCreation", 1f);
    }
    
    [PunRPC]
    void ContinueCreation()
    {
        Planet.OnPlanetLighted += PlanetLighted;
        CreateMyCharacter();

        Invoke("PlaceCharacterRandom", .5f);

        if (PhotonNetwork.isMasterClient)
        {
            PowersManager.LoadPowers();
            PhotonNetwork.Instantiate("Money/icon", Vector3.zero, Quaternion.identity, 0);
            Invoke("CreateAIs", .3f);
            MapTransform.gameObject.AddComponent<PlanetFreezerF>();
            photonView.RPC("ContinueCreation", PhotonTargets.Others);
        }
    }

    public static void CreateMyCharacter()
    {
        string CharacterType = PlayerPrefs.GetString("CharacterType");

        if (PhotonNetwork.inRoom)
        MyCharacter = PhotonNetwork.Instantiate("Characters/" + CharacterType + "/character", Character.StartPosition, Quaternion.identity, 0).GetComponent<Character>();

        MyCharacter.CreatePC();
        MyCharacter.CreatePM();

        MyCharacter.SetName(MyInfo.name);
        MyCharacter.Type = (CharacterTypes)Enum.Parse(typeof(CharacterTypes), CharacterType);
        SetMyCharacter(MyCharacter);
    }

    public static void SetMyCharacter(Character Character)
    {
        if(MyCharacter != null)
        {
            MyCharacter.IsManualControlled = false;
        }
        MyCharacter = Character;
        MyCharacter.IsManualControlled = true;

        CameraController.cameraController.SetCharacter(MyCharacter);
        UIController.uiController.SetCharacter(MyCharacter);
    }

    void CreateAIs()
    {
        string[] types = Enum.GetNames(typeof(CharacterTypes));
        for (int i = PhotonNetwork.room.playerCount + 1; i <= 4; i ++)
        {
            int type = UnityEngine.Random.Range(0, types.Length);
            Character character = PhotonNetwork.Instantiate("Characters/" + types[type] + "/character", Character.StartPosition, Quaternion.identity, 0).GetComponent<Character>();
            character.CreatePC();
            character.CreatePM();
            character.gameObject.AddComponent<AICharacter>();
        }
    }
    
    void PlaceCharacterRandom()
    {
        foreach(Character character in Characters)
            if(character.photonView.isMine)
                character.PM.SetOn(((IEnumerable<Planet>)Planet.Planets.Values).PickRandom());
    }

    public static void GoToPlanet(GameObject PlanetObj)
	{
		PathFinderResult result = MyCharacter.PM.pathFinder.FindPath(PlanetObj.GetComponent<Planet>());

        if (result == PathFinderResult.Forbidden)
            UIController.Assert("Forbidden");
        else if (result == PathFinderResult.Failed)
            UIController.Assert("Impossible");
        else
            MyCharacter.PM.GoToPathFinderList();
	}

    public static void ShowPath(GameObject PlanetObj)
    {
        gameC.UnlightPath();

        if (MyCharacter.PM.pathFinder.LastResult == PathFinderResult.Succeded)
        {
            TurnLights(MyCharacter.PM.pathFinder.Path, 3);
            gameC.Invoke("UnlightPath", 1f);
        }
    }

    void UnlightPath()
    {
        CancelInvoke("UnlightPath");
        UnlightAllPlanets();
    }
    
    public void PlanetLighted(Planet planet, int light)
    {
        if (light == 0)
            LightedPlantes.Remove(planet);
        else
            LightedPlantes.Add(planet);
    }

    public void UnlightAllPlanets()
    {
        foreach (Planet planet in LightedPlantes.ToArray())
            planet.LightPlanet(0);
    }

    public static void TurnLights(IList<IStation> stations, int light)
    {
        foreach (IStation station in stations)
        {
            try { ((Planet)station).LightPlanet(light); } catch { }
        }
    }

    public static IStation GetRandomStation()
    {
        return Stations[UnityEngine.Random.Range(0, Stations.Count - 1)];
    }

    public static T GetRandom<T>(IEnumerable<T> List)
    {
        return List.ElementAt(UnityEngine.Random.Range(0, List.Count() - 1));
    }
	
	public void ArriveToBigPlanet()
	{
        photonView.RPC("FocusCameraOnBigPlanet", PhotonTargets.All);
		Invoke("CallLoadScoreLevel", 4f);
	}

    [PunRPC]
    void FocusCameraOnBigPlanet()
    {
        if (PhotonNetwork.isMasterClient)
        {
            photonView.RPC("FocusCameraOnBigPlanet", PhotonTargets.Others);
            FinishPlanet.finishplanet.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
            GravityController.gravityController.StopAllCoroutines();
            foreach(Character p in Characters)
            {
                if (p.photonView.isMine)
                {
                    Destroy(p.PM);
                    Destroy(p);
                }
            }
        }
        FinishPlanet.finishplanet.enabled = false;
        MyCharacter.PM.enabled = false;
        CameraController.cameraController.StopFollowingCharacter();
        CameraController.cameraController.FollowedTarget = FinishPlanet.finishplanet.transform;
    }

    [PunRPC]void CallLoadScoreLevel()
	{
		if(PhotonNetwork.isMasterClient)
			photonView.RPC ("LoadScoreLevel", PhotonTargets.All);
		else
			photonView.RPC ("CallLoadScoreLevel", PhotonTargets.MasterClient);
			
	}

	[PunRPC] void LoadScoreLevel()
    {
        PhotonNetwork.LeaveRoom();
	}

	public override void OnMasterClientSwitched (PhotonPlayer newMasterClient)
	{
		MyInfo.MasterDissconnected = true;
		photonView.RPC ("LoadScoreLevel", PhotonTargets.All);
	}

	public override void OnDisconnectedFromPhoton ()
	{
		MyInfo.dissconnected = true;
		OnLeftRoom ();
	}

	public override void OnPhotonPlayerDisconnected(PhotonPlayer Character)
	{    
		if (PhotonNetwork.room.playerCount == 1)
			LoadScoreLevel ();	
	}

	void OnDestroy()
	{
        Characters.Clear();
		Stations.Clear ();

        Planet.ClearData ();
        AIManager.ClearData();
        PowersManager.ClearData();
        FIPlanet.ClearData();
	}

	public override void OnLeftRoom()
	{
		Application.LoadLevel (0);
	}

    void OnApplicationPause(bool pauseStatus)
    {
        if(pauseStatus == true)
        {
            PhotonNetwork.LeaveRoom();
        }
    }

}

public struct Maps
{
    public int Xmin, Ymin, Xmax, Ymax;
} 
