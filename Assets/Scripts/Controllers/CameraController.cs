using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CameraController : MonoBehaviour
{    
    public static CameraController cameraController;

    public float RefocusDistancePerFrame = 0.1f;
    public float sensibility = -0.02f;

    public bool canFollow = true, canExplore = true, canManualResize = true, isExploring;

    public new Camera camera;
    public Transform FollowedTarget;
    Character FollowedCharacter;

    void Awake()
    {
        cameraController = this;
        camera = GetComponent<Camera>();
        wantedSize = camera.orthographicSize;
        InputManager.CharacterEvent.OnDrag += DragCharacter;
        InputManager.CharacterEvent.OnDrop += DropCharacter;
    }

    public void SetCharacter(Character Character)
    {
        if (FollowedCharacter != null)
            StopFollowingCharacter();

        FollowedCharacter = Character;
        StartFollowingCharacter();
    }

    void CharacterArrive(IStation from, IStation to)
    {
        FollowedTarget = to.Transform;
    }

    void CharacterLeave(IStation from, IStation to)
    {
        FollowedTarget = FollowedCharacter.transform;
    }

    public void StartFollowingCharacter()
    {
        FollowedTarget = FollowedCharacter.transform;
        FollowedCharacter.PM.OnPArrive += CharacterArrive;
        FollowedCharacter.PM.OnPArrive += CharacterLeave;
    }

    public void StopFollowingCharacter()
    {
        FollowedCharacter.PM.OnPArrive -= CharacterArrive;
        FollowedCharacter.PM.OnPArrive -= CharacterLeave;
    }

    void Update()
    {
        if (wantedSize != camera.orthographicSize)
        {
            float elapsedTime = Time.time - startTime;
            if (elapsedTime < resizeingTime)
                camera.orthographicSize = Mathf.Lerp(camera.orthographicSize, wantedSize, (Time.time - startTime) / resizeingTime);
            else
                camera.orthographicSize = wantedSize;

            if (wantedSize == camera.orthographicSize)
                canManualResize = true;
        }

        if (canExplore && !exploreCanceled && IsExploring())
            ManualExplore();
        else
        {
            if(canManualResize)
                CheckResizeing();

            if (canFollow)
                FollowTarget();
        }
    }

    void FollowTarget()
    {
        if (FollowedTarget == null)
            return;

        Vector2 distance = FollowedTarget.position - transform.position;

        if (distance.magnitude < RefocusDistancePerFrame)
            transform.Translate(distance);
        else
            transform.Translate(distance.normalized * RefocusDistancePerFrame);
    }

    void ManualExplore()
    {
        transform.position = (Vector2)transform.position + Input.touches[0].deltaPosition * sensibility;
    }

    bool exploreCanceled = false;

    bool IsExploring()
    {
        if (Input.touchCount != 1)
        {
            if (isExploring == true)
            {
                canFollow = false;
                Invoke("ResetFollowTarget", 1f);
            }
            return isExploring = false;
        }

        if (isExploring)
            return true;
        
        CancelInvoke("ResetFollowTarget");
        return isExploring = true;
    }

    public void DragCharacter(GameObject sender, GameObject On, Vector2 Position)
    {
        if (sender != FollowedCharacter.gameObject)
            return;
        if (isExploring)
        {
            isExploring = false;
            exploreCanceled = true;
        }
    }

    public void DropCharacter(GameObject sender)
    {
        if (sender != FollowedCharacter.gameObject)
            return;
        if (exploreCanceled)
        {
            isExploring = false;
            exploreCanceled = false;
        }
    }

    void ResetFollowTarget()
    {
        canFollow = true;
    }

    #region Resize

    float oldFingersDistance = 0, startTime, resizeingTime, wantedSize;

    public float minSize = 10, maxSize = 14;

    void CheckResizeing()
    {
        if (Input.touchCount != 2)
        {
            oldFingersDistance = 0;
            return;
        }

        float distance = Vector2.Distance(Input.touches[0].position, Input.touches[1].position);

        if (oldFingersDistance == 0)
            oldFingersDistance = Vector2.Distance(Input.touches[0].position, Input.touches[1].position);

        camera.orthographicSize = wantedSize = Mathf.Clamp(camera.orthographicSize * (oldFingersDistance / distance), minSize, maxSize);
        oldFingersDistance = distance;
    }

    public void Resize(float newSize, float time)
    {
        canManualResize = false;
        resizeingTime = time;
        wantedSize = Mathf.Clamp(newSize, minSize, maxSize);
        startTime = Time.time;
    }

    #endregion
}
