using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class UIController : Photon.MonoBehaviour {

	public static UIController uiController;

	public List<DragablePoision> poisionList = new List<DragablePoision>();
	public List<Text> LogField = new List<Text> ();

	public Text PointsField;

	public static Color Green;
	public static Color Red;

	public GameObject MainMenu;
	public GameObject DetailsMenu;
	public GameObject ExitMenu;

    Character Character;

	private bool AnotherMenuOpened = false;

	void Awake()
	{
		uiController = this;
	}

	public void SetUp()
	{
        DetailsMenu = Instantiate(Resources.Load<GameObject>("Menu/Details"));
        DetailsMenu.SetActive(false);
        DetailsMenu.GetComponent<DetailsController>().OnExit += DetailsExit;
        PointsField.text = StartController.Points.ToString();
        StartController.OnPointsChanged += RefreshPointsField;
        
		foreach (DragablePoision poision in poisionList) {
			poision.OnRemove += RemovePow;
		}
	}

    public void SetCharacter(Character character)
    {
        if(Character != null)
        {
            Character.PC.PowersChanged -= RefreshPoisionList;
        }

        Character = character;
        Character.PC.PowersChanged += RefreshPoisionList;

        RefreshPoisionList();
    }
    
    
	void RefreshPointsField(int OldValue, int newValue)
	{
		if (newValue == OldValue)
			return;

		string message = (newValue - OldValue).ToString();

		if (newValue > OldValue) {
			LogMessage ("+" + message,Green);
		} else
			LogMessage ( message,Red);
		PointsField.text = newValue.ToString();
	}


	public static void LogMessage(string Message, Color Col)
	{
		uiController.StopAllCoroutines ();

		Deepen ();

		uiController.LogField[0].text = Message;
		uiController.LogField [0].color = Col;

		StartCorutines ();
	}

	private static void Deepen()
	{
		for (int i = uiController.LogField.Count - 2; i>=0 ; i--) {
			uiController.LogField [i + 1].text = uiController.LogField [i].text;
			uiController.LogField [i + 1].color = uiController.LogField [i].color;
		}
	}

	private static void StartCorutines()
	{
		foreach (Text t in uiController.LogField)
		{
			uiController.StartCoroutine (uiController.MessageDisappear (t,t.color, Color.clear));
		}

	}


	IEnumerator MessageDisappear (Text Field,Color start,Color stop)
	{
		int nr = 0;
		Field.color = start;
		while (Field.color.a != 0) {
			Field.color = Color.Lerp(start,stop, 0.02f * nr);
			nr++;
			yield return new WaitForSeconds(0.05f);
		}
	}

	public void RemovePow(DragablePoision poision)
	{
		int pos = poisionList.IndexOf(poision);
		if (pos != 0) {
			Character.PC.PowersQue.RemoveAt (pos-1);
		} else {
			if(Character.PC.ActualPower!=null)
				Character.PC.ActualPower.StopPow();
		}
		RefreshPoisionList ();
	}

	void RefreshPoisionList()
	{
        if (Character.PC.ActualPower == null)
            poisionList[0].Disable();
        else
        {
            poisionList[0].image.sprite = PowersManager.IconDex[Character.PC.ActualPower.Type].GetSprite();
            poisionList[0].Refresh();
        }
		for (int i=1; i<poisionList.Count; i++)
			if (i > Character.PC.PowersQue.Count)
				poisionList [i].Disable ();
			else {
				poisionList [i].image.sprite = PowersManager.IconDex[Character.PC.PowersQue [i-1]].GetSprite();
				poisionList[i].Refresh();
			}
	}

    public static void Assert(string message)
    {
		LogMessage (message,Color.white);
    }

	public void TheButtonPressed(bool MainButton)
	{
        if (!MainButton)
        {
            if (!AnotherMenuOpened && MainMenu.activeInHierarchy)
                MainMenu.SetActive(false);
        }
        else if (!AnotherMenuOpened)
        {
            MainMenu.SetActive(!MainMenu.GetActive());
        }

        if (MainMenu.GetActive())
        {
            CameraController.cameraController.canExplore = false;
            CameraController.cameraController.canManualResize = false;
            InputManager.inputManager.enabled = false;
        }
        else
        {
            CameraController.cameraController.canExplore = true;
            CameraController.cameraController.canManualResize = true;
            InputManager.inputManager.enabled = true;
        }
    }

	public void ExitPressed()
	{
		AnotherMenuOpened = true;
		ExitMenu.SetActive (true);
		MainMenu.SetActive (false);
    }

	public void DetailsPressed()
	{
        AnotherMenuOpened = true;
		MainMenu.SetActive (false);
		DetailsMenu.SetActive (true);
	}

	public void DetailsExit()
	{
		AnotherMenuOpened = false;
		MainMenu.SetActive (true);
	}

	public void SureExit()
	{
		PhotonNetwork.LeaveRoom ();
	}

	public void BackExit()
	{
		if (ExitMenu.activeInHierarchy) {
			AnotherMenuOpened = false;
			ExitMenu.SetActive (false);
			MainMenu.SetActive (true);
		}
	}
}
