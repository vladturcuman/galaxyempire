using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GravityController : Photon.MonoBehaviour {
    
	const float speed = 2f;
    public static float MaxDistance = 40f;

    public static IList<IRotatable> RotatableObjects = new List<IRotatable>();

    public static GravityController gravityController;

	public void Start()
	{
        gravityController = this;
        if(!PhotonNetwork.isMasterClient)
        {
            Destroy(this);
        }
        else
        {
            foreach (IRotatable p in RotatableObjects)
                p.RigidBody.AddForce(Vector2.up * Random.Range(1f, 30f));
        }
	}
    
    void FixedUpdate()
	{
        foreach (IRotatable p in RotatableObjects)
        { 
            if (Vector3.Distance(transform.position, p.Transform.position) > MaxDistance)
            {
                p.RigidBody.velocity = Vector2.zero;
                p.RigidBody.AddForce((transform.position - p.Transform.position) * 0.05f, ForceMode2D.Impulse);
            }
            else
            {
                p.Transform.RotateAround(transform.position, Vector3.forward, speed / (Vector3.Distance(transform.position, p.Transform.position)));
            }
        }
		
	}

    void OnDestroy()
    {
        RotatableObjects.Clear();
    }
}
