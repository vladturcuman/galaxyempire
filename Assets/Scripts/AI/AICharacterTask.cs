using System;
using UnityEngine;

public abstract class AICharacterTask : MonoBehaviour
{
    public abstract AICharacterTaskTypes TaskType { get; }

    public Character Character { get; internal set; }

    public delegate void Action(AICharacterTaskResult result);
    public abstract event Action OnStoped;

    public abstract void StartTask(Character character);
    public abstract void Refresh();

    public virtual void StopTask()
    {
        StopTask(AICharacterTaskResult.Forced);
    }

    public abstract void StopTask(AICharacterTaskResult mess);
}

public enum AICharacterTaskResult{
	Succeded,
	Failed,
	Forced
}

public enum AICharacterTaskTypes{
    GetMoney,
    GetPowers,
    UsePlanetMover
}