using System;
using System.Collections.Generic;
using UnityEngine;

public static class AIManager
{
	public static IList<Character> Characters = new List<Character> ();
    public static IDictionary<PowerTypes, PowerIcon> PowerIconDex;

    public static void SetUp()
    {
        PowerIconDex = PowersManager.IconDex;
        Character.OnAwaked += OnCharacterAwaked;
    }

    public static void ClearData()
    {
        Characters.Clear();
        Character.OnAwaked -= OnCharacterAwaked;
    }

    public static void OnCharacterAwaked(Character p)
    {
        Characters.Add(p);
    }
}
