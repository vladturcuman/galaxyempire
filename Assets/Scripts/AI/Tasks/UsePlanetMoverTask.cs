using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;

namespace AI.Tasks
{
    class UsePlanetMoverTask : AICharacterTask
    {
        public override AICharacterTaskTypes TaskType { get { return AICharacterTaskTypes.UsePlanetMover; } }
        public override event Action OnStoped;

        const float TimeLimit = 3f;

        private List<Character> Targets;

        private IStation target;   
        

        public override void StartTask(Character character)
        {
            Character = character;
            if (Character.PC.ActualPower == null || Character.PC.ActualPower.Type != PowerTypes.PlanetMover)
            {
                StopTask(AICharacterTaskResult.Succeded);
                return;
            }

            Targets = AIManager.Characters.Where(x => x != Character && x.GetStation() != null).ToList();
            target = Targets[UnityEngine.Random.Range(0, Targets.Count)].GetStation();
            InvokeRepeating("ShakeIt", 0f, 1.2f);
        }

        private void ShakeIt()
        {
            if(Character.PC.ActualPower != null  && Character.PC.ActualPower.Type == PowerTypes.PlanetMover )
            ((Powers.PlanetMover)(Character.PC.ActualPower)).Drag( target.Transform.gameObject,
                                                                target.Transform.gameObject,
                                                                RandomPosition(target.Transform.position));
            else
                StopTask(AICharacterTaskResult.Succeded);
        }

        Vector2 RandomPosition(Vector2 tmp)
        {
            return new Vector2(  max(SunPlanet.SunPosition.x - 50f, min(SunPlanet.SunPosition.x + 50f, UnityEngine.Random.Range(tmp.x - 25f, tmp.x + 25f))),
                                 max(SunPlanet.SunPosition.y - 50f, min(SunPlanet.SunPosition.y + 50f, UnityEngine.Random.Range(tmp.y - 25f, tmp.y + 25f))));
                              
        }

        public override void Refresh()
        {
            target = null;
            try { CancelInvoke("ShakeIt"); } catch { }
        }
        
        public override void StopTask(AICharacterTaskResult result)
        {
            if (Character.PC.ActualPower != null && Character.PC.ActualPower.Type == PowerTypes.PlanetMover)
                Character.PC.ActualPower.StopPow();
            Refresh();
            if (OnStoped != null)
                OnStoped(result);
        }

        float min(float a, float b) { return a < b ? a : b; }
        float max(float a, float b) { return a > b ? a : b; }

    }

}
