using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class GetPowerTask : AICharacterTask
{
    public override AICharacterTaskTypes TaskType { get { return AICharacterTaskTypes.GetPowers; } }
    public override event Action OnStoped;

    PowerIcon lastPowerIcon;
    int nrPowers;

    public override void StartTask(Character character)
    {
        Character = character;

        nrPowers = Character.PC.PowersQue.Count;
        if (Character.PC.ActualPower != null)
            nrPowers++;

        Character.PM.OnEjected += FindPower;
        Character.PM.OnPArrive += CharacterArrived;
        Character.PC.PowersChanged += PowersQueChanged;
        FindPower();
    }

    public override void Refresh()
    {
        Character.PM.DeleteJunk();
        if (lastPowerIcon != null)
            lastPowerIcon.OnMoved -= PowerIconMoved;
        lastPowerIcon = null;
        try { CancelInvoke("FindPower"); } catch { }
    }
    
    public override void StopTask(AICharacterTaskResult mess)
    {
        Refresh();
        Character.PM.OnPArrive -= CharacterArrived;
        Character.PM.OnEjected -= FindPower;
        Character.PC.PowersChanged -= PowersQueChanged;
        if (OnStoped != null)
            OnStoped(mess);
    }

    void FindPower()
    {
        if (this == null)
            return;

        Refresh();

        PowerIcon powerIcon = GetBestPowerIcon();
        if (powerIcon != null && powerIcon.Planet != null)
            if (Character.PM.pathFinder.FindPath(powerIcon.Planet) == PathFinderResult.Succeded)
            {
                lastPowerIcon = powerIcon;
                powerIcon.OnMoved += PowerIconMoved;
                Character.PM.GoToPathFinderList();
                if (lastPowerIcon.Planet == Character.PM.PCurrentStation)
                    CharacterArrived(lastPowerIcon.Planet, lastPowerIcon.Planet);
                return;
            }

        NothingToDo();
    }

    PowerIcon GetBestPowerIcon()
    {
        if (AIManager.PowerIconDex.Count == 0)
            return null;

        float bestDist = 1000000, d;
        PowerIcon best = null;

        foreach (PowerIcon powerIcon in AIManager.PowerIconDex.Values)
        {
            if (powerIcon.Planet == null)
                continue;
            d = Vector2.Distance(powerIcon.Planet.Transform.position, Character.transform.position);
            if (d < bestDist) {best = powerIcon; bestDist = d;}
        }

        return best;
    }

    void NothingToDo()
    {
        if (this == null)
            return;

        if (Character.GetStation() != null && Character.GetStation().Type == StationTypes.City)
        {
            IStation BestPlanet = null;

            foreach (IStation Planet in GameController.Stations.Values.Where(x => (x.Type != StationTypes.City && Character.PM.CanIGoTo(x))))
            {
                BestPlanet = Planet;
                break;
            }

            if (BestPlanet != null)
                Character.PM.GoTo(BestPlanet);
        }
        Invoke("FindPower", Random.Range(0.3f, 1.5f));
    }

    void PowerIconMoved(PowerIcon powerIcon)
    {
        Refresh();
        NothingToDo();
    }

    void CharacterArrived(IStation from, IStation to)
    {
        if (lastPowerIcon != null && lastPowerIcon.Planet == (object)to)
        {
            Character.PM.RotateTo(lastPowerIcon.transform, false);
        }
    }

    void PowersQueChanged()
    {
        if (nrPowers <= Character.PC.PowersQue.Count)
            StopTask(AICharacterTaskResult.Succeded);
    }
}
