using UnityEngine;
using System.Collections.Generic;
using AI.Tasks;

public class AICharacter : Photon.PunBehaviour
{
    public Character Character { get; internal set; }
	public const int moneyLimit = 10;
    
    AICharacterTask ActualTask;

	void Awake()
	{
        if (photonView.isMine)
        {
            Character = GetComponent<Character>();
            InitialSetUp();
            InvokeRepeating("RoutineCheck", 3f, 5f);
        }
	}

	void RoutineCheck()
	{
        if (Character.PM.pathFinder.FindPath(FinishPlanet.finishplanet) == PathFinderResult.Succeded)
        {
            StopCurrentTask();
            Character.PM.GoToPathFinderList();
        }
        else
            if (ActualTask == null)
                StartNewTask();
	}

	void InitialSetUp()
	{
        Character.CreatePM();
        Character.CreatePC();
		Character.SetName ("AI");
        Character.PM.OnMovementPermisionChanged += MovePermisionChanged;
    }

    void MovePermisionChanged(bool canMove)
    {
        StopCurrentTask();
        if (canMove)
            StartNewTask();
    }

    public void StopCurrentTask()
    {
        if (ActualTask == null)
            return;
        ActualTask.OnStoped -= TaskStoped;
        Destroy(ActualTask);
        ActualTask = null;
    }

	void StartNewTask()
	{
		ActualTask = GetNewTask (); 
		ActualTask.OnStoped += TaskStoped;
		ActualTask.StartTask (Character);
	}

	AICharacterTask GetNewTask()
	{
		if (Character.PC.ActualPower == null || Character.PC.ActualPower.Type == PowerTypes.SuperJump)
		    return gameObject.AddComponent<GetPowerTask>();

        if (Character.PC.ActualPower.Type == PowerTypes.PlanetMover)
			return gameObject.AddComponent<UsePlanetMoverTask>();

        return gameObject.AddComponent<GetPowerTask>();
	}

    void TaskStoped(AICharacterTaskResult result)
    {
        if (result != AICharacterTaskResult.Forced)
        {
            Destroy(ActualTask);
            StartNewTask();
        }
	}
}
