using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PrimitivePlanet : Planet {

    public float Radius;

    void Awake()
    {
        radius = Radius;
        id = GameController.Stations.Count;
        GameController.Stations.Add(id, this);
        Visitors = new List<Character>();
    }

    void Start()
	{
		trailCost = 0;
	}
}
