using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class FIPlanet : Planet, IFIPlanet
{
    public static IList<FIPlanet> FIPlanets = new List<FIPlanet>();

    #region FIPlanet
    public float DistanceFromSun { get { return Vector2.Distance(SunPlanet.SunPosition, transform.position); } }

    internal override void Awake()
    {
        base.Awake();
        FIPlanets.Add(this);
    }

    public virtual void Freez()
    {
        Refresh();
        photonView.RPC("ChangePlanetState", PhotonTargets.All, PlanetStates.Frozen);
    }

    public virtual void Fired()
    {
        Refresh();
        photonView.RPC("ChangePlanetState", PhotonTargets.All, PlanetStates.Fired);
    }

    public virtual void Normalize()
    {
        Refresh();
        photonView.RPC("ChangePlanetState", PhotonTargets.All, PlanetStates.Normal);
    }

    [PunRPC]
    public virtual void Freez(float time)
    {
        if (!PhotonNetwork.isMasterClient)
        {
            photonView.RPC("Freez", PhotonTargets.MasterClient, time);
            return;
        }
        Freez();
        InvokeRepeating("TryNormalize", time, time);
    }

    [PunRPC]
    public virtual void Fired(float time)
    {
        if (!PhotonNetwork.isMasterClient)
        {
            photonView.RPC("Fired", PhotonTargets.MasterClient, time);
            return;
        }
        Fired();
        InvokeRepeating("TryNormalize", time, time);
    }

    void TryNormalize()
    {
        foreach (IFICharacter Character in Visitors)
            if (!(Character.State.Type == CharacterStates.Normal || Character.State.Type == CharacterStates.Fired || Character.State.Type == CharacterStates.Frozen))
                return;
        Normalize();
    }

    public void TryNormalize(float after)
    {
        Refresh();
        if (State == PlanetStates.Normal)
            return;
        InvokeRepeating("TryNormalize", after, after);
    }

    void Refresh()
    {
        try { CancelInvoke("TryNormalize"); } catch { };
    }

    internal override void Sincronize(Character character)
    {
        if (character is IFICharacter)
            Sincronize((IFICharacter)character);
    }

    internal void Sincronize(IFICharacter character)
    {
        switch (character.State.Type)
        {
            case CharacterStates.Normal:
                if (State == PlanetStates.Frozen)
                    character.Freez();
                else if (State == PlanetStates.Fired)
                    character.Fired();
                break;

            case CharacterStates.Ice:
                if (State == PlanetStates.Fired)
                    Normalize();
                else if (State != PlanetStates.Frozen)
                    Freez(5);
                break;

            case CharacterStates.Fire:
                if (State == PlanetStates.Frozen)
                    Normalize();
                else if (State != PlanetStates.Fired)
                    Fired(5);
                break;

            default:
                if (State == PlanetStates.Fired && character.State.Type != CharacterStates.Fired)
                    character.Fired();
                else if (State == PlanetStates.Frozen && character.State.Type != CharacterStates.Frozen)
                    character.Freez();
                else if (State == PlanetStates.Normal)
                    character.Normalize();
                break;
        }
    }

    [PunRPC]
    internal override void ChangePlanetState(PlanetStates state)
    {
        base.ChangePlanetState(state);

        foreach (Character character in Visitors.ToArray())
            if (character.photonView.isMine)
            {
                if (state == PlanetStates.Normal)
                    if (character.State.Type == CharacterStates.Fire || character.State.Type == CharacterStates.Ice)
                    {
                        character.Normalize();
                        continue;
                    }

                Sincronize(character);
            }
    }
    #endregion

    public override bool CanStayOn(Character character)
    {
        if (character is IFICharacter)
            return CanStayOn((IFICharacter)character);
        else
            return true;
    }

    public bool CanStayOn(IFICharacter character)
    {
        return true;
    }

    public static new void ClearData()
    {
        FIPlanets.Clear();
    }
}