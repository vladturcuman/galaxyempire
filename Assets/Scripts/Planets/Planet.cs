using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class Planet : Photon.PunBehaviour, IStation
{
    #region Planet
    public static Sprite[] Lights;
    public static IDictionary<int, Planet> Planets = new Dictionary<int, Planet>();
    public static event Action<Planet, int> OnPlanetLighted;

    public PlanetStates State { get { return state; } }

    internal SpriteRenderer backRenderer, bodyRenderer;
    internal IDictionary<PlanetStates, Sprite[]> Animations = new Dictionary<PlanetStates, Sprite[]>();

    internal PlanetStates state;
    internal string spritesPath;
    Sprite[] currentAnimation;

    internal virtual void Awake()
    {
        Visitors = new List<Character>();
        id = GameController.Stations.Count;
        GameController.Stations.Add(id, this);
        Planets.Add(id, this);
        transform.SetParent(GameController.MapTransform);

        LoadAnimations();
        bodyRenderer = GetComponent<SpriteRenderer>();
        backRenderer = GetComponentsInChildren<SpriteRenderer>()[1];

        LightPlanet(0);
    }

    internal virtual void LoadAnimations()
    {
        var type = typeof(PlanetStates);
        foreach (PlanetStates state in Enum.GetValues(typeof(PlanetStates)))
        {
            try
            {
                Animations.Add(state, Resources.LoadAll<Sprite>(spritesPath +
                                        ((StringValue)(type.GetMember(state.ToString())[0].GetCustomAttributes(true)[0])).Value));
            }
            catch { }
        }
    }

    [PunRPC]
    internal virtual void ChangePlanetState(PlanetStates state)
    {
        this.state = state;
        try { StopCoroutine("Animate"); } catch { }
        currentAnimation = Animations[state];

        if (currentAnimation.Length > 1)
            StartCoroutine("Animate");
        else
            bodyRenderer.sprite = currentAnimation[0];
    }

    public virtual void LightPlanet(int light)
    {
        backRenderer.sprite = Lights[light];
        if(OnPlanetLighted != null)
            OnPlanetLighted(this, light);
    }

    internal virtual IEnumerator Animate()
    {
        for (int i = 0; i < currentAnimation.Length; i++)
        {
            bodyRenderer.sprite = currentAnimation[i];
            if (i == currentAnimation.Length - 1)
                i = -1;

            yield return new WaitForSeconds(0.2f);
        }
    }

    public static void SetUp()
    {
        Lights = Resources.LoadAll<Sprite>("Planets/lights");
    }

    public static void ClearData()
    {
        Lights = null;
        Planets.Clear();
        OnPlanetLighted = null;
    }

    #endregion

    #region  IStation
    public float Radius { get { return radius; } }
    public int ID { get { return id; } }
    public Transform Transform { get { return transform; } }
    public int TrailCost { get { return trailCost; } }
    public StationTypes Type { get { return type; } }
    public IList<Character> Visitors { get; internal set; }

    internal int trailCost = 0;
    internal float radius = 0;
    internal int id;
    internal StationTypes type;

    public virtual void CharacterArrive(Character Character)
    {
        Visitors.Add(Character);
        Character.OnStateChanged += Sincronize;
        Sincronize(Character);

    }

    public virtual void CharacterLeave(Character Character)
    {
        Visitors.Remove(Character);
        Character.OnStateChanged -= Sincronize;
    }

    internal virtual void Sincronize(Character Character)
    {

    }

    public virtual bool CanStayOn(Character Character)
    {
        return true;
    }
    #endregion
}

public enum PlanetStates
{
    [StringValue("normal")]
    Normal,
    [StringValue("frozen")]
    Frozen,
    [StringValue("fired")]
    Fired
}