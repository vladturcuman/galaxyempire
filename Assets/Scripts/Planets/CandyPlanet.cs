using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CandyPlanet : FIPlanet, IRotatable
{
    
    public Rigidbody2D RigidBody { get { return rigidBody; } }
    Rigidbody2D rigidBody;

	internal override void Awake ()
    {
        type = StationTypes.Candy;
        radius = .4f;
        spritesPath = "Planets/Candy/";
        trailCost = 1;
        rigidBody = GetComponent<Rigidbody2D>();
        GravityController.RotatableObjects.Add(this);

        base.Awake();
    }

    internal override void Sincronize(Character Character)
    {
        if (Character is IFatable && State == PlanetStates.Normal)
            if (Character.State.Type == CharacterStates.Fat)
                return;
            else if (Character.State.Type != CharacterStates.Fire && Character.State.Type != CharacterStates.Ice)
            {
                ((IFatable)Character).GetFat();
                return;
            }

        base.Sincronize(Character);
    }

    public override void CharacterLeave(Character Character)
    {
        base.CharacterLeave(Character);

        if (Character.State.Type == CharacterStates.Fat)
            Character.Normalize();
    }
}
