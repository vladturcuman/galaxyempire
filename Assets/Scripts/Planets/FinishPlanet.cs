using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FinishPlanet : Planet
{
    Vector3 distance;
    float minDist = 1f;
	public float speed;
	public Vector2 StartPoint, WaitPoint, PointToGo;

	public static FinishPlanet finishplanet;

    internal override void Awake()
	{
        id = GameController.Stations.Count;
        GameController.Stations.Add(id, this);
        transform.SetParent(GameController.MapTransform);

        LoadAnimations();
        bodyRenderer = GetComponent<SpriteRenderer>();
        backRenderer = GetComponentsInChildren<SpriteRenderer>()[1];

        LightPlanet(0);

        finishplanet = this;
		trailCost = 0;
        transform.position = StartPoint;
		radius = 1.5f;
        type = StationTypes.Finish;

        if (PhotonNetwork.isMasterClient)
        {
            Invoke("StartMoving", 100f);
            PointToGo = StartPoint;
            transform.position = StartPoint;
        }
    }

    void StartMoving()
    {
        StartCoroutine(Move());
    }

    void ChangePointToGo()
    {
        PointToGo = new Vector3(Random.Range(SunPlanet.sunPlanet.transform.position.x - GravityController.MaxDistance * 3 + Radius,
            SunPlanet.sunPlanet.transform.position.x + GravityController.MaxDistance * 3 - Radius),
            Random.Range(SunPlanet.sunPlanet.transform.position.y - GravityController.MaxDistance * 3 + Radius,
            SunPlanet.sunPlanet.transform.position.y + GravityController.MaxDistance * 3 - Radius), 0f);
    }

    IEnumerator Move()
    {
        while (true)
        {
            distance = PointToGo - (Vector2)transform.position;
            while (distance.magnitude < minDist)
            {
                ChangePointToGo();
                distance = PointToGo - (Vector2)transform.position;
            }
            GetComponent<Rigidbody2D>().MovePosition(transform.position + distance.normalized * Time.deltaTime * speed);
            yield return Time.deltaTime;
        }
    }

	public override void CharacterArrive(Character Character)
	{
        GameController.MyCharacter.PM.MovementPermission = false;
        if (Character == GameController.MyCharacter)
            StartController.Points += 10;
        GameController.gameC.ArriveToBigPlanet();
        
	}
}
