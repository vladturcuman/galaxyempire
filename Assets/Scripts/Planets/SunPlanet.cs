using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class SunPlanet : Planet
{
    public static Vector3 SunPosition = new Vector3(0, 0, 0);
    
    short actualPhase = 0;
    short timeToGive = 3;
    Sprite[] spritesArray;
    
    public short ActualPhase { get { return actualPhase; } }

    IList<KeyValuePair<float, PlanetStates>> phases = new List<KeyValuePair<float, PlanetStates>>() {
        new KeyValuePair<float, PlanetStates>( 20, PlanetStates.Fired),
        new KeyValuePair<float, PlanetStates>( 15, PlanetStates.Fired),
        new KeyValuePair<float, PlanetStates>( 20, PlanetStates.Frozen),
        new KeyValuePair<float, PlanetStates>( 20, PlanetStates.Normal)};
    
	public static SunPlanet sunPlanet;

	public delegate void ActionPhase(int phase);
	public event ActionPhase OnPhaseChanged;

	internal override void Awake()
	{
        id = GameController.Stations.Count;
        GameController.Stations.Add(id, this);
        transform.SetParent(GameController.MapTransform);
        Visitors = new List<Character>();

        LoadAnimations();
        bodyRenderer = GetComponent<SpriteRenderer>();
        backRenderer = GetComponentsInChildren<SpriteRenderer>()[1];

        LightPlanet(0);

        sunPlanet = this;
		trailCost = 2;
        type = StationTypes.Sun;
        state = PlanetStates.Fired;
		radius = 1.8f;
		
        if (PhotonNetwork.isMasterClient)
        {
			StartCoroutine (ChangePhase ());

            if (OnPhaseChanged!=null)
				OnPhaseChanged(actualPhase);

            transform.position = SunPosition;
        }

    }
    
	IEnumerator ChangePhase()
	{
		while (true)
        {
            yield return new WaitForSeconds(phases[actualPhase].Key);

            if (++ actualPhase == 4)
				actualPhase = 0;
			photonView.RPC ("ChangePhase", PhotonTargets.All, actualPhase);

            if (OnPhaseChanged != null)
				OnPhaseChanged(actualPhase);
        }
	}

	[PunRPC]
    void ChangePhase(short newPhase)
	{
		actualPhase = newPhase;
        state = phases[actualPhase].Value;
        bodyRenderer.sprite = spritesArray[actualPhase];

        if (actualPhase == 3)
            InvokeRepeating("GiveMoney", 0f, timeToGive);
        else if (actualPhase == 0)
            CancelInvoke("GiveMoney");

        foreach (Character character in Visitors)
            if (character.photonView.isMine)
                Sincronize(character);
	}

    internal override void LoadAnimations()
    {
        spritesArray = Resources.LoadAll<Sprite>("Planets/Sun/phases");
    }

    internal override void ChangePlanetState(PlanetStates state)
    {

    }

    void GiveMoney()
    {
        foreach (Character visitor in Visitors)
            if (visitor == GameController.MyCharacter)
                StartController.Points++;
                
    }

    internal override void Sincronize(Character character)
    {
        if (character is IFICharacter)
            Sincronize((IFICharacter)character);
    }

    void Sincronize(IFICharacter character)
    {
        switch (State)
        {
            case PlanetStates.Fired:
                if (character.State.Type == CharacterStates.Ice)
                    character.Normalize();
                else if (character.State.Type != CharacterStates.Fire && character.State.Type != CharacterStates.Fired)
                    character.Fired();
                break;

            case PlanetStates.Frozen:
                if (character.State.Type == CharacterStates.Fire)
                    character.Normalize();
                else if (character.State.Type != CharacterStates.Ice && character.State.Type != CharacterStates.Frozen)
                    character.Freez();
                break;

            case PlanetStates.Normal:
                if (character.State.Type != CharacterStates.Normal)
                {
                    if(character is FICharacter)
                        ((FICharacter)character).PC.PowersQue.Clear();
                    character.Normalize();
                }
                break;
        }
    }

}

