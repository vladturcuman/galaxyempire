using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CityPlanet : FIPlanet, IRotatable
{
	static float timeToTakePoints = 10;
	static int pointsToTake = 1;
    public Rigidbody2D RigidBody { get { return rigidBody; } }
    Rigidbody2D rigidBody;

    internal override void Awake()
    {
        radius = .2f;
        spritesPath = "Planets/City/";
        type = StationTypes.City;
        rigidBody = GetComponent<Rigidbody2D>();
        GravityController.RotatableObjects.Add(this);

        base.Awake();
	}

	public override void CharacterArrive(Character Character)
	{
        if(Visitors.Count == 0)
		    InvokeRepeating ("TakePoints", timeToTakePoints, timeToTakePoints);
		base.CharacterArrive (Character);
	}

	public override void CharacterLeave(Character Character)
	{
        base.CharacterLeave(Character);
        if (Visitors.Count == 0)
		    CancelInvoke ("TakePoints");
	}

	void TakePoints()
	{
        foreach (Character visitor in Visitors)
            if (visitor == GameController.MyCharacter)
                StartController.Points--;
	}
}