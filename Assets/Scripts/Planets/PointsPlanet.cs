﻿using System.Collections;
using System.Linq;
using UnityEngine;

public class PointsPlanet : Planet
{
    int lifeCycleInterval = 8;
    int lifeTime = 10;
    bool IsActive { get { return transform.localScale.x == 1; } }

    internal override void Awake()
    {
        base.Awake();

        trailCost = 0;
        radius = 1f;
        type = StationTypes.Points;

        if (PhotonNetwork.isMasterClient)
        {
            StartCoroutine(Appear());
        }
    }

    void ChangePosition()
    {
        Vector3 position;
        do
        {
            position = new Vector2(Random.Range(SunPlanet.SunPosition.x - GravityController.MaxDistance, SunPlanet.SunPosition.x + GravityController.MaxDistance),
                Random.Range(SunPlanet.SunPosition.y - GravityController.MaxDistance, SunPlanet.SunPosition.y + GravityController.MaxDistance));
        } while (position.x - SunPlanet.SunPosition.x < 5 && position.y - SunPlanet.SunPosition.y < 5);

        transform.position = position;
    }

    IEnumerator Appear()
    {
        ChangePosition();

        Vector3 target = new Vector3(1, 1, 1);
        float elapsedTime = 0;
        while (elapsedTime < 1)
        {
            transform.localScale = Vector3.Lerp(transform.localScale, target, elapsedTime);
            elapsedTime += Time.deltaTime;
            yield return null;
        }
        transform.localScale = target;

        yield return new WaitForSeconds(lifeTime);
        StartCoroutine(Disappear());
    }

    IEnumerator Disappear()
    {
        float elapsedTime = 0;
        while (elapsedTime < 1)
        {
            transform.localScale = Vector3.Lerp(transform.localScale, Vector3.zero, elapsedTime);
            elapsedTime += Time.deltaTime;
            yield return null;
        }
        transform.localScale = Vector3.zero;

        yield return new WaitForSeconds(lifeCycleInterval);
        StartCoroutine(Appear());
    }

    public override void CharacterArrive(Character Character)
    {
        if (Character == GameController.MyCharacter)
            StartController.Points += Random.Range(1, 4);
        Character.transform.SetParent(null);
        Character.PM.Anchor = null;
        Character.PM.GoBack();
        StopAllCoroutines();
        StartCoroutine(Disappear());
    }

    public override bool CanStayOn(Character Character)
    {
        if (!base.CanStayOn(Character))
            return false;
        return IsActive;
    }
}