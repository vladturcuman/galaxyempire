using UnityEngine;
using System.Collections.Generic;

public class FlowerPlanet : FIPlanet, IRotatable
{
    public Rigidbody2D RigidBody { get { return rigidBody; } }
    Rigidbody2D rigidBody;

    internal override void Awake ()
	{
        type = StationTypes.Flower;
        trailCost = 1;
        spritesPath = "Planets/Flower/";
        radius = .2f;
        rigidBody = GetComponent<Rigidbody2D>();
        GravityController.RotatableObjects.Add(this);

        base.Awake();
	}

}
