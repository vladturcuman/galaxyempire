using System;
using UnityEngine;
using System.Collections.Generic;
using System.Collections;
public class CloseMenuBar : MonoBehaviour 
{
	public delegate void Action();
	public event Action OnExit;
	public Transform Obj;
	Vector2 startPoz,finalPoz,newPoz;
	bool firstTime = true;
	public bool isMoving;

	void Start()
	{
		Invoke ("GetPos", 0.1f);
	}

	void GetPos()
	{
		float h = ((RectTransform)Obj.transform).rect.height;
		((RectTransform)Obj.transform).offsetMin = new Vector2 (0f, h);
		((RectTransform)Obj.transform).offsetMax = new Vector2 (0f, h);
		finalPoz = Obj.position;
		startPoz = new Vector2 (finalPoz.x, finalPoz.y - h);
		Obj.transform.position = finalPoz;
		StartCoroutine(MoveBack(Obj.transform));
		firstTime = false;
	}

	void OnEnable()
	{
		try{Obj.GetComponentInChildren<Scrollator>().canMove = true;}catch{}
		if (!firstTime) {
			StopAllCoroutines ();
			Obj.position = finalPoz;
			StartCoroutine(MoveBack(Obj));
		}
	}

	public void BeginDrag()
	{
		isMoving = true;
		StopAllCoroutines ();
		try{Obj.GetComponentInChildren<Scrollator>().canMove = false;}catch{}
	}

	public void Drag()
	{
		newPoz = Input.GetTouch(0).position;
		newPoz = new Vector2(startPoz.x,Mathf.Clamp (newPoz.y,startPoz.y,finalPoz.y));
		Obj.position = newPoz;
	}

	public void EndDrag()
	{
		if (finalPoz.y - Obj.position.y > Obj.position.y - startPoz.y )
			StartCoroutine (MoveBack (Obj));
		else
			StartCoroutine (MoveUp (Obj));
	}

	IEnumerator MoveBack(Transform tr)
	{
		tr.position = new Vector2(startPoz.x,Mathf.Lerp(tr.position.y,startPoz.y,5 * Time.deltaTime));
		while (tr.position.y - startPoz.y > 1f) {
			yield return null;
			tr.position = new Vector2(startPoz.x,Mathf.Lerp(tr.position.y,startPoz.y,5 * Time.deltaTime));
		}
		isMoving = false;
		tr.position = startPoz;
		try{Obj.GetComponentInChildren<Scrollator>().canMove = true;}catch{}
	}

	IEnumerator MoveUp(Transform tr)
	{
		tr.position = new Vector2(startPoz.x,Mathf.Lerp(tr.position.y,finalPoz.y,5 * Time.deltaTime));
		while (finalPoz.y - tr.position.y > 1f) {
			yield return null;
			tr.position = new Vector2(startPoz.x,Mathf.Lerp(tr.position.y,finalPoz.y,5 * Time.deltaTime));
		}
		tr.position = finalPoz;
		isMoving = false;
		try{Obj.GetComponentInChildren<Scrollator>().canMove = true;}catch{}
		if (OnExit != null)
			OnExit ();
	}

	public void MoveBack()
	{
		
	}

	public void MoveUp()
	{
		StopAllCoroutines ();
		StartCoroutine (MoveUp (Obj));
	}
}

